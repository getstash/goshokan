# Builder image, where we build the example.
FROM golang:1.11-alpine3.7 AS builder

# Dependencies for build
RUN apk --no-cache add make

WORKDIR /go/src/goshokan
COPY . .
RUN make build

# Final image.
FROM alpine

RUN apk --no-cache add bash ca-certificates tzdata
ENV TZ Europe/Minsk

WORKDIR "/goshokan"
COPY --from=builder /go/src/goshokan/server .
COPY --from=builder /go/src/goshokan/pocket_import .
CMD ["/goshokan/server"]
