package main

import (
	"goshokan/pkg"
)

func main() {
	app := pkg.NewGoshokan()
	_, err := app.GetPocketImportUseCase().Execute()
	if err != nil {
		app.GetLogger().ErrorWithMessage(err, "Error while importing from pocket")
		return
	}
}
