package main

import "goshokan/pkg"

func main() {
	app := pkg.NewGoshokan()
	app.GetHttpV2Service().Run(true, "5000")
}
