package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/models"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/services"
)

type GetBoardHandler struct {
	getBoardUseCase *usecases.GetBoardUseCase
	logger          interfaces.ILogger
}

func (h *GetBoardHandler) Handle(params board.GetBoardBoardIDParams) middleware.Responder {

	token, err := services.GetTokenFromCookie(params.HTTPRequest)
	if err != nil {
		h.logger.ErrorWithMessage(err, "GetTokenFromCookie")
		return board.NewGetBoardBoardIDInternalServerError()
	}

	useCaseResponse, err := h.getBoardUseCase.Execute(params.BoardID, token)
	if err == interfaces.ErrNoBoard {
		return board.NewGetBoardBoardIDNotFound()
	}
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return board.NewGetBoardBoardIDUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "getBoardUseCase.Execute")
		return board.NewGetBoardBoardIDInternalServerError()
	}

	linkModels := []*models.Link{}
	for _, entity := range useCaseResponse.Links {
		linkModels = append(linkModels, DomainLinkToLinkPresentation(entity))
	}

	response := board.NewGetBoardBoardIDOK()
	response.Payload = &models.BoardWithLinks{
		ID:     &useCaseResponse.ID,
		Name:   &useCaseResponse.Name,
		Links:  linkModels,
		IsOpen: useCaseResponse.IsOpen,
	}
	return response
}

func NewGetBoardHandler(getBoardUseCase *usecases.GetBoardUseCase, logger interfaces.ILogger) *GetBoardHandler {
	return &GetBoardHandler{
		getBoardUseCase: getBoardUseCase,
		logger:          logger,
	}
}
