package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/services"
)

type DeleteBoardHandler struct {
	deleteBoardUseCase *usecases.DeleteBoardUseCase
	logger             interfaces.ILogger
}

func (h *DeleteBoardHandler) Handle(params board.DeleteBoardBoardIDParams) middleware.Responder {

	token, err := services.GetTokenFromCookie(params.HTTPRequest)
	if err != nil {
		h.logger.ErrorWithMessage(err, "GetTokenFromCookie")
		return board.NewDeleteBoardBoardIDInternalServerError()
	}

	usecaseErr := h.deleteBoardUseCase.Execute(params.BoardID, token)
	if usecaseErr == interfaces.ErrNoBoard {
		return board.NewDeleteBoardBoardIDNotFound()
	}
	if usecaseErr != nil {

		h.logger.ErrorWithMessage(usecaseErr, "deleteBoardUseCase.Execute")
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			return board.NewDeleteBoardBoardIDUnauthorized()
		}
		return board.NewDeleteBoardBoardIDInternalServerError()
	}

	return board.NewDeleteBoardBoardIDOK()
}

func NewDeleteBoardHandler(useCase *usecases.DeleteBoardUseCase, logger interfaces.ILogger) *DeleteBoardHandler {
	return &DeleteBoardHandler{
		deleteBoardUseCase: useCase,
		logger:             logger,
	}
}
