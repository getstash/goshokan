package handlers

import (
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/restapi/operations/login"
	"goshokan/pkg/infrastructure/http/services"
	"net/http"
)

type UserInfo struct {
	Name     string
	Password string
}

type GetLoginOK struct {
	cookie *http.Cookie
}

// NewGetLoginOK creates GetAuthOK with default headers values
func NewGetLoginOK(cookie *http.Cookie) *GetLoginOK {

	return &GetLoginOK{cookie}
}

// WriteResponse to the client
func (o *GetLoginOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses
	http.SetCookie(rw, o.cookie)

	rw.WriteHeader(200)

}

type LoginHandler struct {
	loginUseCase     *usecases.LoginUseCase
	logger           interfaces.ILogger
	CookieLifeTimeMs int
}

func (h *LoginHandler) Handle(params login.GetLoginParams, principal interface{}) middleware.Responder {

	userInfo, ok := principal.(*UserInfo)
	if !ok {
		return login.NewGetLoginInternalServerError()
	}

	token, err := h.loginUseCase.Execute(userInfo.Name, userInfo.Password)
	if err == interfaces.ErrWrongCredentials {
		return login.NewGetLoginUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "loginUseCase.Execute")
		return login.NewGetLoginInternalServerError()
	}

	return NewGetLoginOK(services.BuildTokenCookie(token, h.CookieLifeTimeMs))
}

func NewLoginHandler(loginUseCase *usecases.LoginUseCase, logger interfaces.ILogger, CookieLifeTimeMs int) *LoginHandler {
	return &LoginHandler{
		loginUseCase:     loginUseCase,
		logger:           logger,
		CookieLifeTimeMs: CookieLifeTimeMs,
	}
}
