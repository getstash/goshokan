package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/models"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/services"
)

type GetBoardsHandler struct {
	findBoardsUseCase *usecases.FindBoardsUseCase
	logger            interfaces.ILogger
}

func (h *GetBoardsHandler) Handle(params board.GetBoardParams) middleware.Responder {
	token, err := services.GetTokenFromCookie(params.HTTPRequest)
	if err != nil {
		h.logger.ErrorWithMessage(err, "GetTokenFromCookie")
		return board.NewGetBoardInternalServerError()
	}

	entites, err := h.findBoardsUseCase.Execute(token)
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return board.NewGetBoardUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "findBoardsUseCase.Execute")
		return board.NewGetBoardInternalServerError()
	}

	payload := models.GetBoardOKBody{}
	modelsList := []*models.Board{}
	for _, entity := range entites {
		linksCount := int64(len(entity.LinkIDs))
		modelsList = append(
			modelsList,
			&models.Board{
				ID:         &entity.ID,
				Name:       &entity.Name,
				LinksCount: &linksCount,
				IsOpen:     &entity.IsOpen,
			},
		)
	}
	payload.Boards = modelsList
	response := board.NewGetBoardOK()
	response.Payload = &payload

	return response
}

func NewGetBoardsHandler(findBoardsUseCase *usecases.FindBoardsUseCase, logger interfaces.ILogger) *GetBoardsHandler {
	return &GetBoardsHandler{
		findBoardsUseCase: findBoardsUseCase,
		logger:            logger,
	}
}
