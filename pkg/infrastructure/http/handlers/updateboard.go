package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/models"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/services"
)

type UpdateBoardHandler struct {
	updateBoardUseCase *usecases.UpdateBoardUseCase
	logger             interfaces.ILogger
}

func (h *UpdateBoardHandler) Handle(params board.PostBoardBoardIDParams) middleware.Responder {
	token, tokenErr := services.GetTokenFromCookie(params.HTTPRequest)
	if tokenErr != nil {
		h.logger.ErrorWithMessage(tokenErr, "GetTokenFromCookie")
		return board.NewPostBoardBoardIDInternalServerError()
	}

	ids := entities.StringsListToLinkIDList(params.Board.LinkIds)
	name := ""
	if params.Board.Name != nil {
		name = *params.Board.Name
	}

	req := interfaces.UpdateBoardUseCaseRequest{
		NewBoard: interfaces.NewBoard{
			Name:    name,
			LinkIDs: ids,
			IsOpen:  params.Board.IsOpen,
		},
		ID:    params.BoardID,
		Token: token,
	}

	useCaseResponse, err := h.updateBoardUseCase.Execute(&req)
	if err == interfaces.ErrNoBoard {
		return board.NewPostBoardBoardIDNotFound()
	}
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return board.NewPostBoardBoardIDUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "updateBoardUseCase.Execute")
		return board.NewPostBoardBoardIDInternalServerError()
	}

	linkModels := []*models.Link{}
	for _, entity := range useCaseResponse.Links {
		linkModels = append(linkModels, DomainLinkToLinkPresentation(entity))
	}
	response := board.NewPostBoardBoardIDOK()
	response.Payload = &models.BoardWithLinks{
		ID:     &useCaseResponse.ID,
		Name:   &useCaseResponse.Name,
		Links:  linkModels,
		IsOpen: useCaseResponse.IsOpen,
	}
	return response
}

func NewUpdateBoardHandler(updateBoardUseCase *usecases.UpdateBoardUseCase, logger interfaces.ILogger) *UpdateBoardHandler {
	return &UpdateBoardHandler{updateBoardUseCase: updateBoardUseCase, logger: logger}
}
