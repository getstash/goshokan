package handlers

import (
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/infrastructure/http/restapi/operations/login"
	"goshokan/pkg/infrastructure/http/services"
	"net/http"
)

type PostLogoutOK struct {
}

// NewPostLogoutOK creates PostLogoutOK with default headers values
func NewPostLogoutOK() *PostLogoutOK {

	return &PostLogoutOK{}
}

// WriteResponse to the client
func (o *PostLogoutOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.Header().Del(runtime.HeaderContentType) //Remove Content-Type on empty responses
	services.DeleteTokenCookie(rw)

	rw.WriteHeader(200)
}

type PostLogoutHandler struct{}

func (h *PostLogoutHandler) Handle(login.PostLogoutParams) middleware.Responder {
	return NewPostLogoutOK()
}
