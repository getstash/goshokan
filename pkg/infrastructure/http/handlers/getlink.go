package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/models"
	"goshokan/pkg/infrastructure/http/restapi/operations/link"
	"goshokan/pkg/infrastructure/http/services"
)

func pI32topI(value *int32) *int {
	var resultValue *int

	if value != nil {
		i32Value := *value
		iValue := int(i32Value)
		resultValue = &iValue
	}

	return resultValue
}

type GetLinkHandler struct {
	searchLinksUseCase *usecases.SearchLinksUseCase
	logger             interfaces.ILogger
}

func (h *GetLinkHandler) Handle(params link.GetLinkParams) middleware.Responder {

	token, err := services.GetTokenFromCookie(params.HTTPRequest)
	if err != nil {
		h.logger.ErrorWithMessage(err, "GetTokenFromCookie")
		return link.NewGetLinkInternalServerError()
	}

	pattern := ""
	if params.Q != nil {
		pattern = *params.Q
	}

	limit := pI32topI(params.Limit)
	offset := pI32topI(params.Offset)

	resp, err := h.searchLinksUseCase.Execute(pattern, limit, offset, token)
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return link.NewGetLinkUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "searchLinksUseCase.Execute")
		return link.NewGetLinkInternalServerError()
	}

	modelLinks := []*models.Link{}

	for _, link := range resp {
		modelLinks = append(modelLinks, DomainLinkToLinkPresentation(link))
	}

	payload := &models.GetLinkOKBody{
		Links: modelLinks,
	}

	return &link.GetLinkOK{
		Payload: payload,
	}
}

func NewGetLinkHandler(useCase *usecases.SearchLinksUseCase, logger interfaces.ILogger) *GetLinkHandler {
	return &GetLinkHandler{
		searchLinksUseCase: useCase,
		logger:             logger,
	}
}
