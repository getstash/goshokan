package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/services"
)

type PostBoardHandler struct {
	addNewBoardUseCase *usecases.AddNewBoardUseCase
	logger             interfaces.ILogger
}

func (h *PostBoardHandler) Handle(params board.PostBoardParams) middleware.Responder {
	token, tokenErr := services.GetTokenFromCookie(params.HTTPRequest)
	if tokenErr != nil {
		h.logger.ErrorWithMessage(tokenErr, "GetTokenFromCookie")
		return board.NewPostBoardInternalServerError()
	}

	ids := entities.StringsListToLinkIDList(params.Board.LinkIds)
	name := ""
	if params.Board.Name != nil {
		name = *params.Board.Name
	}

	req := interfaces.AddNewBoardUseCaseRequest{
		NewBoard: interfaces.NewBoard{
			Name:    name,
			LinkIDs: ids,
			IsOpen:  params.Board.IsOpen,
		},
		Token: token,
	}

	err := h.addNewBoardUseCase.Execute(&req)
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return board.NewPostBoardUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "addNewBoardUseCase.Execute")
		return board.NewPostBoardInternalServerError()
	}
	return board.NewPostBoardCreated()
}

func NewPostBoardHandler(addNewBoardUseCase *usecases.AddNewBoardUseCase, logger interfaces.ILogger) *PostBoardHandler {
	return &PostBoardHandler{
		addNewBoardUseCase: addNewBoardUseCase,
		logger:             logger,
	}
}
