package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/restapi/operations/board"
	"goshokan/pkg/infrastructure/http/restapi/operations/link"
	"goshokan/pkg/infrastructure/http/services"
)

type PostLinkHandler struct {
	addNewLinkUseCase *usecases.AddNewLinksUseCase
	logger            interfaces.ILogger
}

func (h *PostLinkHandler) Handle(params link.PostLinkParams) middleware.Responder {
	token, tokenErr := services.GetTokenFromCookie(params.HTTPRequest)
	if tokenErr != nil {
		h.logger.ErrorWithMessage(tokenErr, "GetTokenFromCookie")
		return board.NewPostBoardBoardIDInternalServerError()
	}

	protoLinks := []*interfaces.ProtoLink{}
	for _, newLink := range params.Links {
		protoLinks = append(protoLinks, ModelNewLinkToProtoLink(newLink))
	}

	req := interfaces.AddNewLinkUseCaseRequest{
		ProtoLinks: protoLinks,
		Token:      token,
	}

	err := h.addNewLinkUseCase.Execute(&req)
	_, ok := err.(*domainServices.TokenAuthError)
	if ok {
		return link.NewPostLinkUnauthorized()
	}
	if err != nil {
		h.logger.ErrorWithMessage(err, "addNewLinkUseCase.Execute")
		return &link.PostLinkInternalServerError{}
	}

	return &link.PostLinkCreated{}
}

func NewPostLinkHandler(useCase *usecases.AddNewLinksUseCase, logger interfaces.ILogger) *PostLinkHandler {
	return &PostLinkHandler{
		addNewLinkUseCase: useCase,
		logger:            logger,
	}
}
