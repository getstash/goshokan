package handlers

import (
	"github.com/go-openapi/strfmt"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/infrastructure/http/models"
)

func DomainLinkToLinkPresentation(link *entities.Link) *models.Link {
	dt := strfmt.NewDateTime()
	dt.Scan(link.Added)

	//tagsList :=
	tagsList := []string{}
	for _, tag := range link.Tags {
		tagsList = append(tagsList, string(tag))
	}

	modelLink := &models.Link{
		ID:         string(link.ID),
		ExternalID: link.ExternalID,
		Added:      dt,
		Name:       link.Name,
		Source:     int64(link.Source),
		Tags:       tagsList,
		URL:        link.Url,
		Excerpt:    link.Excerpt,
		ImageURL:   link.ImageUrl,
	}
	return modelLink
}

func ModelNewLinkToProtoLink(newLink *models.NewLink) *interfaces.ProtoLink {

	name := ""
	if newLink.Name != nil {
		name = *newLink.Name
	}

	url := ""
	if newLink.URL != nil {
		url = *newLink.URL
	}

	return &interfaces.ProtoLink{
		Name: name,
		Url:  url,
		Tags: newLink.Tags,
	}
}
