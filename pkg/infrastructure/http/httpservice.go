package http

import (
	"github.com/go-openapi/loads"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http/handlers"
	"goshokan/pkg/infrastructure/http/restapi"
	"goshokan/pkg/infrastructure/http/restapi/operations"
	"log"
)

type HTTPService struct {
	server *restapi.Server
}

func (s *HTTPService) Run(port int) {
	s.server.Host = "0.0.0.0"
	s.server.Port = port

	if err := s.server.Serve(); err != nil {
		log.Fatalln(err)
	}
}

func NewHTTPService(
	addNewLinksUseCase *usecases.AddNewLinksUseCase,
	addNewBoardUseCase *usecases.AddNewBoardUseCase,
	findBoardsUseCase *usecases.FindBoardsUseCase,
	getBoardUseCase *usecases.GetBoardUseCase,
	searchLinksUseCase *usecases.SearchLinksUseCase,
	deleteBoardUseCase *usecases.DeleteBoardUseCase,
	updateBoardUseCase *usecases.UpdateBoardUseCase,
	loginUseCase *usecases.LoginUseCase,
	CookieLifeTimeMs int,
	logger interfaces.ILogger,
) *HTTPService {
	service := &HTTPService{}

	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewHttpkanAPI(swaggerSpec)
	service.server = restapi.NewServer(api)

	postLinkHandler := handlers.NewPostLinkHandler(addNewLinksUseCase, logger)
	postBoardHandler := handlers.NewPostBoardHandler(addNewBoardUseCase, logger)
	getBoardsHandler := handlers.NewGetBoardsHandler(findBoardsUseCase, logger)
	getBoardHandler := handlers.NewGetBoardHandler(getBoardUseCase, logger)
	getLinkHandler := handlers.NewGetLinkHandler(searchLinksUseCase, logger)
	deleteBoardHandler := handlers.NewDeleteBoardHandler(deleteBoardUseCase, logger)
	postBoardIDHandler := handlers.NewUpdateBoardHandler(updateBoardUseCase, logger)
	loginHandler := handlers.NewLoginHandler(loginUseCase, logger, CookieLifeTimeMs)
	logoutHandler := &handlers.PostLogoutHandler{}

	api.LinkGetLinkHandler = getLinkHandler
	api.LinkPostLinkHandler = postLinkHandler
	api.BoardPostBoardHandler = postBoardHandler
	api.BoardGetBoardHandler = getBoardsHandler
	api.BoardGetBoardBoardIDHandler = getBoardHandler
	api.BoardDeleteBoardBoardIDHandler = deleteBoardHandler
	api.BoardPostBoardBoardIDHandler = postBoardIDHandler
	api.LoginGetLoginHandler = loginHandler
	api.LoginPostLogoutHandler = logoutHandler

	service.server.ConfigureAPI()

	return service
}
