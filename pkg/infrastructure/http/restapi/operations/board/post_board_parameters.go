// Code generated by go-swagger; DO NOT EDIT.

package board

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"io"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	models "goshokan/pkg/infrastructure/http/models"
)

// NewPostBoardParams creates a new PostBoardParams object
// no default values defined in spec.
func NewPostBoardParams() PostBoardParams {

	return PostBoardParams{}
}

// PostBoardParams contains all the bound params for the post board operation
// typically these are obtained from a http.Request
//
// swagger:parameters PostBoard
type PostBoardParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*new board to be created
	  Required: true
	  In: body
	*/
	Board *models.NewBoard
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewPostBoardParams() beforehand.
func (o *PostBoardParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	if runtime.HasBody(r) {
		defer r.Body.Close()
		var body models.NewBoard
		if err := route.Consumer.Consume(r.Body, &body); err != nil {
			if err == io.EOF {
				res = append(res, errors.Required("board", "body"))
			} else {
				res = append(res, errors.NewParseError("board", "body", "", err))
			}
		} else {

			// validate body object
			if err := body.Validate(route.Formats); err != nil {
				res = append(res, err)
			}

			if len(res) == 0 {
				o.Board = &body
			}
		}
	} else {
		res = append(res, errors.Required("board", "body"))
	}
	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
