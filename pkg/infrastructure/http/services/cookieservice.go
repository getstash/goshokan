package services

import (
	"net/http"
	"time"
)

var TokenCookieName = "Token"

func GetTokenFromCookie(req *http.Request) (string, error) {
	cookie, err := req.Cookie(TokenCookieName)
	if err != nil {
		return "", nil
	}

	return cookie.Value, nil
}

func BuildTokenCookie(token string, cookieLifetimeMs int) *http.Cookie {
	expire := time.Now().Add(time.Duration(cookieLifetimeMs) * time.Millisecond)
	cookie := http.Cookie{Name: TokenCookieName, Value: token, Path: "/", Expires: expire, MaxAge: cookieLifetimeMs}
	return &cookie
}

func DeleteTokenCookie(rw http.ResponseWriter) {
	cookie := http.Cookie{Name: TokenCookieName, Value: "", Path: "/", MaxAge: -1}
	http.SetCookie(rw, &cookie)
}
