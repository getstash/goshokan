package config

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"strings"
)

const SearchEngineElasticSearch = "elastic"
const SearchEngineBleve = "bleve"

var SearchEngineChoices = map[string]bool{
	SearchEngineElasticSearch: true,
	SearchEngineBleve:         true,
}

const StorageEngineElasticSearch = "elastic"
const StorageEngineBoltDB = "boltdb"

var StorageEngineChoices = map[string]bool{
	StorageEngineElasticSearch: true,
	StorageEngineBoltDB:        true,
}

const DefaultSearchEnginePath = "./search_engine"
const DefaultStorageEnginePath = "./storage_engine"

// DefaultCookieLifeTimeMs will be used as default value for CookieLifeTimeMs
const DefaultCookieLifeTimeMs = 7 * 24 * 60 * 60 * 1000
const DefaultAdminUsername = "admin"
const DefautlAdminPassword = "password"

type Config struct {
	Debug            bool   `required:"true"`
	ElasticsearchURI string `envconfig:"kan_elastic_url" required:"true"`

	Port string `envconfig:"kan_server_port" required:"true"`

	PocketAccessToken string `envconfig:"kan_pocket_access_token" required:"false"`
	PocketConsumerKey string `envconfig:"kan_pocket_consumer_key" required:"false"`

	CookieLifeTimeMs int `envconfig:"kan_cookie_lifetime" required:"false"`

	SecretKey string `envconfig:"secret_key" required:"true"`

	AdminUsername string `envconfig:"admin_username" required:"false"`
	AdminPassword string `envconfig:"admin_password" required:"false"`

	SearchEngine  string `envconfig:"search_engine" required:"true"`
	StorageEngine string `envconfig:"storage_engine" required:"true"`

	SearchEngineStoragePath  string `envconfig:"search_engine_storage_path" required:"false"`
	StorageEngineStoragePath string `envconfig:"storage_engine_storage_path" required:"false"`

	CORSAllowedOrigin string `envconfig:"cors_allowed_origin" required:"false"`
}

func ReadConfig() (*Config, error) {
	config := &Config{}
	err := envconfig.Process("kan", config)
	if err != nil {
		return nil, err
	}

	if config.CookieLifeTimeMs == 0 {
		config.CookieLifeTimeMs = DefaultCookieLifeTimeMs
	}
	if config.AdminUsername == "" {
		config.AdminUsername = DefaultAdminUsername
	}
	if config.AdminPassword == "" {
		config.AdminPassword = DefautlAdminPassword
	}

	_, searchEngineExists := SearchEngineChoices[config.SearchEngine]
	if !searchEngineExists {
		return nil, fmt.Errorf("not existing choice for search_engine field: %s", config.SearchEngine)
	}

	_, storageEngineExists := StorageEngineChoices[config.StorageEngine]
	if !storageEngineExists {
		return nil, fmt.Errorf("not existing choice for storage_engine field: %s", config.StorageEngine)
	}

	if config.SearchEngineStoragePath == "" {
		config.SearchEngineStoragePath = DefaultSearchEnginePath
	}

	if config.StorageEngineStoragePath == "" {
		config.StorageEngineStoragePath = DefaultStorageEnginePath
	}

	if !(strings.HasPrefix(config.CORSAllowedOrigin, "http://") || strings.HasPrefix(config.CORSAllowedOrigin, "https://")) {
		return nil, fmt.Errorf("bad origin %s. Should start from http:// or https://", config.CORSAllowedOrigin)
	}

	return config, nil
}
