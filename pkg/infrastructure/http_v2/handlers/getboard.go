package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"net/http"
)

type GetBoardHandler struct {
	getBoardUseCase *usecases.GetBoardUseCase
	isPublic        bool
	logger interfaces.ILogger
}

func NewGetBoardHandler(getBoardUseCase *usecases.GetBoardUseCase, isPublic bool, logger interfaces.ILogger) *GetBoardHandler {
	handler := &GetBoardHandler{}
	handler.getBoardUseCase = getBoardUseCase
	handler.isPublic = isPublic
	handler.logger = logger
	return handler
}

func (h *GetBoardHandler) Handle(c *gin.Context) {
	boardId := c.Param("boardId")

	token := ""

	if !h.isPublic {
		newToken, tokenErr := utils.GetTokenFromHeader(c)
		if tokenErr != nil {
			utils.UnauthorizedResponse(c)
			return
		}
		token = newToken
	}

	useCaseResponse, usecaseErr := h.getBoardUseCase.Execute(boardId, token)
	if usecaseErr == interfaces.ErrNoBoard {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}
	_, ok := usecaseErr.(*domainServices.TokenAuthError)
	if ok {
		utils.UnauthorizedResponse(c)
		return
	}
	if usecaseErr != nil {
		h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. getBoardUseCase %s", usecaseErr))
		utils.InternalServerErrorResponse(c)
		return
	}

	linkModels := []*models.LinkModel{}
	for _, entity := range useCaseResponse.Links {
		linkModels = append(linkModels, models.DomainLinkToLinkPresentation(entity))
	}

	c.JSON(http.StatusOK, models.BoardWithLinks{
		ID:     &useCaseResponse.ID,
		Name:   &useCaseResponse.Name,
		Links:  linkModels,
		IsOpen: useCaseResponse.IsOpen,
	})
}
