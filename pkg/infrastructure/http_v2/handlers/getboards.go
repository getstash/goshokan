package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"net/http"
)

type GetBoardsHandler struct {
	findBoardsUseCase *usecases.FindBoardsUseCase
	logger interfaces.ILogger
}

func NewGetBoardsHandler(findBoardsUseCase *usecases.FindBoardsUseCase, logger interfaces.ILogger) *GetBoardsHandler {
	handler := &GetBoardsHandler{}
	handler.findBoardsUseCase = findBoardsUseCase
	handler.logger = logger
	return handler
}

func (h *GetBoardsHandler) Handle(c *gin.Context) {
	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	entites, usecaseErr := h.findBoardsUseCase.Execute(token)
	if usecaseErr == interfaces.ErrNoBoard {
		c.JSON(http.StatusNotFound, gin.H{})
		return
	}
	_, ok := usecaseErr.(*domainServices.TokenAuthError)
	if ok {
		utils.UnauthorizedResponse(c)
		return
	}
	if usecaseErr != nil {
		h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. findBoardsUseCase %s", usecaseErr))
		utils.InternalServerErrorResponse(c)
		return
	}

	modelsList := []*models.Board{}
	for _, entity := range entites {
		linksCount := int64(len(entity.LinkIDs))
		modelsList = append(
			modelsList,
			&models.Board{
				ID:         &entity.ID,
				Name:       &entity.Name,
				LinksCount: &linksCount,
				IsOpen:     &entity.IsOpen,
			},
		)
	}

	c.JSON(http.StatusOK, gin.H{
		"boards": modelsList,
	})
}
