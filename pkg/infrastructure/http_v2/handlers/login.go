package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"net/http"
)

type LoginHandler struct {
	loginUseCase *usecases.LoginUseCase
	logger interfaces.ILogger
}

func NewLoginHandler(loginUseCase *usecases.LoginUseCase, logger interfaces.ILogger) *LoginHandler {
	handler := LoginHandler{}
	handler.loginUseCase = loginUseCase
	handler.logger = logger
	return &handler
}

func (h *LoginHandler) Handle(c *gin.Context) {
	login, password, basicAuthErr := utils.GetLoginAndPasswordFromBasicAuth(c)
	if basicAuthErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	token, usecaseErr := h.loginUseCase.Execute(login, password)
	if usecaseErr != nil {
		if usecaseErr == interfaces.ErrWrongCredentials {
			utils.UnauthorizedResponse(c)
			return
		}
		h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. loginUseCase %s", usecaseErr))
		utils.InternalServerErrorResponse(c)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}
