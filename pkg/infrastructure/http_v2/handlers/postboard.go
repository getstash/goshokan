package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/validator.v9"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"log"
)

type PostBoardHandler struct {
	validator          *validator.Validate
	addNewBoardUseCase *usecases.AddNewBoardUseCase
	logger interfaces.ILogger
}

func NewPostBoardHandler(addNewBoardUseCase *usecases.AddNewBoardUseCase, validate *validator.Validate, logger interfaces.ILogger) *PostBoardHandler {
	handler := &PostBoardHandler{}
	handler.addNewBoardUseCase = addNewBoardUseCase
	handler.validator = validate
	return handler
}

func (h *PostBoardHandler) Handle(c *gin.Context) {
	req := models.NewBoard{}
	bindingErr := c.BindJSON(&req)
	if bindingErr != nil {
		log.Println(bindingErr)
		return
	}

	validationErr := h.validator.Struct(&req)
	if validationErr != nil {
		errors, casted := validationErr.(validator.ValidationErrors)
		if casted {
			utils.UnprocessableEntityResponse(c, errors)
			return
		} else {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. validator NewPostBoardHandler %s", validationErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	ids := entities.StringsListToLinkIDList(req.LinkIds)
	name := ""
	if req.Name != nil {
		name = *req.Name
	}

	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	usecaseReq := interfaces.AddNewBoardUseCaseRequest{
		NewBoard: interfaces.NewBoard{
			Name:    name,
			LinkIDs: ids,
			IsOpen:  req.IsOpen,
		},
		Token: token,
	}

	usecaseErr := h.addNewBoardUseCase.Execute(&usecaseReq)
	if usecaseErr != nil {
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			utils.UnauthorizedResponse(c)
			return
		}
		if usecaseErr != nil {
			log.Println(usecaseErr)
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. addNewBoardUseCase %s", usecaseErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	utils.CreatedResponse(c)
}
