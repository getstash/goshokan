package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/validator.v9"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"log"
)

type postCreateLinksRequest []models.NewLinkModel

type CreateLinksHandler struct {
	validator          *validator.Validate
	addNewLinksUseCase *usecases.AddNewLinksUseCase
	logger interfaces.ILogger
}

func NewCreateLinksHandler(addNewLinksUseCase *usecases.AddNewLinksUseCase, validate *validator.Validate, logger interfaces.ILogger) *CreateLinksHandler {
	handler := &CreateLinksHandler{}
	handler.addNewLinksUseCase = addNewLinksUseCase
	handler.validator = validate
	handler.logger = logger
	return handler
}

func (h *CreateLinksHandler) Handle(c *gin.Context) {
	req := postCreateLinksRequest{}
	err := c.BindJSON(&req)
	if err != nil {
		log.Println(err)
		return
	}

	for index, item := range req {
		err = h.validator.Struct(&item)
		if err != nil {
			errors, casted := err.(validator.ValidationErrors)
			if casted {
				utils.UnprocessableEntityResponseForList(c, errors, index)
				return
			} else {
				h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. validator %s", err))
				utils.InternalServerErrorResponse(c)
				return
			}
		}
	}

	protoLinks := []*interfaces.ProtoLink{}
	for _, newLink := range req {
		protoLinks = append(protoLinks, models.ModelNewLinkToProtoLink(&newLink))
	}

	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	usecaseReq := interfaces.AddNewLinkUseCaseRequest{
		ProtoLinks: protoLinks,
		Token:      token,
	}

	usecaseErr := h.addNewLinksUseCase.Execute(&usecaseReq)
	if usecaseErr != nil {
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			utils.UnauthorizedResponse(c)
			return
		}
		if usecaseErr != nil {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. addNewLinksUseCase %s", usecaseErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	utils.CreatedResponse(c)
}
