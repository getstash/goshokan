package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/validator.v9"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"log"
	"net/http"
)

type UpdateBoardHandler struct {
	validator          *validator.Validate
	updateBoardUseCase *usecases.UpdateBoardUseCase
	logger interfaces.ILogger
}

func NewUpdateBoardHandler(updateBoardUseCase *usecases.UpdateBoardUseCase, validate *validator.Validate, logger interfaces.ILogger) *UpdateBoardHandler {
	handler := &UpdateBoardHandler{}
	handler.updateBoardUseCase = updateBoardUseCase
	handler.validator = validate
	handler.logger = logger
	return handler
}

func (h *UpdateBoardHandler) Handle(c *gin.Context) {
	boardId := c.Param("boardId")
	req := models.NewBoard{}
	bindingErr := c.BindJSON(&req)
	if bindingErr != nil {
		log.Println(bindingErr)
		return
	}

	validationErr := h.validator.Struct(&req)
	if validationErr != nil {
		log.Println(validationErr)
		errors, casted := validationErr.(validator.ValidationErrors)
		if casted {
			utils.UnprocessableEntityResponse(c, errors)
			return
		} else {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. validator NewUpdateBoardHandler %s", validationErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	ids := entities.StringsListToLinkIDList(req.LinkIds)
	name := ""
	if req.Name != nil {
		name = *req.Name
	}

	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	usecaseReq := interfaces.UpdateBoardUseCaseRequest{
		NewBoard: interfaces.NewBoard{
			Name:    name,
			LinkIDs: ids,
			IsOpen:  req.IsOpen,
		},
		ID:    boardId,
		Token: token,
	}

	useCaseResponse, usecaseErr := h.updateBoardUseCase.Execute(&usecaseReq)
	if usecaseErr != nil {
		if usecaseErr == interfaces.ErrNoBoard {
			c.Status(http.StatusNotFound)
			return
		}
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			utils.UnauthorizedResponse(c)
			return
		}
		if usecaseErr != nil {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. updateBoardUseCase %s", validationErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	linkModels := []*models.LinkModel{}
	for _, entity := range useCaseResponse.Links {
		linkModels = append(linkModels, models.DomainLinkToLinkPresentation(entity))
	}

	c.JSON(http.StatusOK, models.BoardWithLinks{
		ID:     &useCaseResponse.ID,
		Name:   &useCaseResponse.Name,
		Links:  linkModels,
		IsOpen: useCaseResponse.IsOpen,
	})
}
