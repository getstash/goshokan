package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"net/http"
)

type DeleteBoardHandler struct {
	deleteBoardUseCase *usecases.DeleteBoardUseCase
	logger interfaces.ILogger
}

func NewDeleteBoardHandler(deleteBoardUseCase *usecases.DeleteBoardUseCase, logger interfaces.ILogger) *DeleteBoardHandler {
	handler := &DeleteBoardHandler{}
	handler.deleteBoardUseCase = deleteBoardUseCase
	handler.logger = logger
	return handler
}

func (h *DeleteBoardHandler) Handle(c *gin.Context) {
	boardId := c.Param("boardId")

	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	usecaseErr := h.deleteBoardUseCase.Execute(boardId, token)
	if usecaseErr != nil {
		if usecaseErr == interfaces.ErrNoBoard {
			c.Status(http.StatusNotFound)
			return
		}
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			utils.UnauthorizedResponse(c)
			return
		}
		if usecaseErr != nil {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. deleteBoardUseCase %s", usecaseErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	utils.OkResponse(c)
}
