package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
	"goshokan/pkg/domain/interfaces"
	domainServices "goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/models"
	"goshokan/pkg/infrastructure/http_v2/utils"
	"log"
	"net/http"
)

type GetSearchLinksRequest struct {
	Pattern string `schema:"q"`
	Limit   *int   `limit:"limit"`
	Offset  *int   `offset:"offset"`
}

type GetLinksHandler struct {
	decoder            *schema.Decoder
	searchLinksUseCase *usecases.SearchLinksUseCase
	logger interfaces.ILogger
}

func NewGetLinksHandler(searchLinksUseCase *usecases.SearchLinksUseCase, decoder *schema.Decoder, logger interfaces.ILogger) *GetLinksHandler {
	handler := &GetLinksHandler{}
	handler.searchLinksUseCase = searchLinksUseCase
	handler.decoder = decoder
	handler.logger = logger
	return handler
}

func (h *GetLinksHandler) Handle(c *gin.Context) {
	var req GetSearchLinksRequest
	err := h.decoder.Decode(&req, c.Request.URL.Query())
	if err != nil {
		errMap, casted := err.(schema.MultiError)
		if casted {
			utils.BadRequestResponseFromSchemaErrors(errMap, c)
			return
		} else {
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. decoder.Decode %s", err))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	token, tokenErr := utils.GetTokenFromHeader(c)
	if tokenErr != nil {
		utils.UnauthorizedResponse(c)
		return
	}

	links, usecaseErr := h.searchLinksUseCase.Execute(req.Pattern, req.Limit, req.Offset, token)
	if usecaseErr != nil {
		_, ok := usecaseErr.(*domainServices.TokenAuthError)
		if ok {
			utils.UnauthorizedResponse(c)
			return
		}
		if usecaseErr != nil {
			log.Println(usecaseErr)
			h.logger.Info(fmt.Sprintf("InternalServerErrorResponse. searchLinksUseCase %s", usecaseErr))
			utils.InternalServerErrorResponse(c)
			return
		}
	}

	modelLinks := []*models.LinkModel{}

	for _, link := range links {
		modelLinks = append(modelLinks, models.DomainLinkToLinkPresentation(link))
	}

	c.JSON(http.StatusOK, gin.H{
		"links": modelLinks,
	})
}
