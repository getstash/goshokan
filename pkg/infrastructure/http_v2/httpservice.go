package http_v2

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
	"gopkg.in/go-playground/validator.v9"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/http_v2/handlers"
	"log"
)

type HTTPService struct {
	router *gin.Engine
}

func NewHTTPService(
	addNewLinksUseCase *usecases.AddNewLinksUseCase,
	addNewBoardUseCase *usecases.AddNewBoardUseCase,
	findBoardsUseCase *usecases.FindBoardsUseCase,
	getBoardUseCase *usecases.GetBoardUseCase,
	getPublicBoardUseCase *usecases.GetBoardUseCase,
	searchLinksUseCase *usecases.SearchLinksUseCase,
	deleteBoardUseCase *usecases.DeleteBoardUseCase,
	updateBoardUseCase *usecases.UpdateBoardUseCase,
	loginUseCase *usecases.LoginUseCase,
	logger interfaces.ILogger,
	corsAllowedOrigin string,
) *HTTPService {
	httpService := &HTTPService{}

	validate := validator.New()
	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)

	getLinksHandler := handlers.NewGetLinksHandler(searchLinksUseCase, decoder, logger)
	createLinksHandler := handlers.NewCreateLinksHandler(addNewLinksUseCase, validate, logger)
	getBoardsHandler := handlers.NewGetBoardsHandler(findBoardsUseCase, logger)
	getBoardHandler := handlers.NewGetBoardHandler(getBoardUseCase, false, logger)
	getPublicBoardHandler := handlers.NewGetBoardHandler(getPublicBoardUseCase, true, logger)
	postBoardHandler := handlers.NewPostBoardHandler(addNewBoardUseCase, validate, logger)
	updateBoardHandler := handlers.NewUpdateBoardHandler(updateBoardUseCase, validate, logger)
	deleteBoardHandler := handlers.NewDeleteBoardHandler(deleteBoardUseCase, logger)
	loginHandler := handlers.NewLoginHandler(loginUseCase, logger)

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = false
	corsConfig.AllowCredentials = true

	if corsAllowedOrigin == "" {
		corsConfig.AllowOriginFunc = func(origin string) bool {
			return true
		}
	} else {
		corsConfig.AllowOrigins = []string{corsAllowedOrigin}
	}

	corsConfig.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type", "Authorization"}
	corsConfig.AllowMethods = []string{"GET", "POST", "PUT", "HEAD", "DELETE"}

	router := gin.Default()
	router.Use(cors.New(corsConfig))

	router.GET("/api/v1/link", getLinksHandler.Handle)
	router.POST("/api/v1/link", createLinksHandler.Handle)

	router.GET("/api/v1/board", getBoardsHandler.Handle)
	router.POST("/api/v1/board", postBoardHandler.Handle)
	router.GET("/api/v1/board/:boardId", getBoardHandler.Handle)
	router.POST("/api/v1/board/:boardId", updateBoardHandler.Handle)
	router.DELETE("/api/v1/board/:boardId", deleteBoardHandler.Handle)

	router.GET("/api/v1/public/board/:boardId", getPublicBoardHandler.Handle)

	router.POST("/api/v1/login", loginHandler.Handle)

	httpService.router = router

	return httpService
}

func (s *HTTPService) Run(isRelease bool, port string) {
	if isRelease {
		gin.SetMode(gin.ReleaseMode)
	}

	err := s.router.Run(fmt.Sprintf(":%s", port))
	if err != nil {
		log.Fatal(err)
	}
}
