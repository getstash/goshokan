package models

// NewBoard new board
// swagger:model NewBoard
type NewBoard struct {

	// is open
	IsOpen bool `json:"isOpen,omitempty"`

	// link ids
	// Required: true
	LinkIds []string `json:"linkIds" validate:"required"`

	// name
	// Required: true
	Name *string `json:"name" validate:"required"`
}
