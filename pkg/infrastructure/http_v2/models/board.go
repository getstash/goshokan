package models

type Board struct {

	// id
	// Required: true
	ID *string `json:"id"`

	// is open
	// Required: true
	IsOpen *bool `json:"isOpen"`

	// links count
	// Required: true
	LinksCount *int64 `json:"linksCount"`

	// name
	// Required: true
	Name *string `json:"name"`
}
