package models

type BoardWithLinks struct {

	// id
	// Required: true
	ID *string `json:"id"`

	// is open
	IsOpen bool `json:"isOpen,omitempty"`

	// links
	// Required: true
	Links []*LinkModel `json:"links"`

	// name
	// Required: true
	Name *string `json:"name"`
}
