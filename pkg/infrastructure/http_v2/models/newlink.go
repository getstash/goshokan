package models

import (
	"goshokan/pkg/domain/interfaces"
)

type NewLinkModel struct {
	// name
	Name *string `json:"name" validate:"required" binding:"required"`

	// tags
	Tags []string `json:"tags"`

	// url
	URL *string `json:"url" validate:"required" binding:"required"`
}

func ModelNewLinkToProtoLink(newLink *NewLinkModel) *interfaces.ProtoLink {

	name := ""
	if newLink.Name != nil {
		name = *newLink.Name
	}

	url := ""
	if newLink.URL != nil {
		url = *newLink.URL
	}

	return &interfaces.ProtoLink{
		Name: name,
		Url:  url,
		Tags: newLink.Tags,
	}
}
