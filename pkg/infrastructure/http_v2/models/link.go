package models

import (
	"goshokan/pkg/domain/entities"
	"time"
)

type LinkModel struct {

	// added
	// Format: date-time
	Added time.Time `json:"added,omitempty"`

	// excerpt
	Excerpt string `json:"excerpt,omitempty"`

	// external Id
	ExternalID string `json:"externalId,omitempty"`

	// id
	// Read Only: true
	ID string `json:"id,omitempty"`

	// image Url
	ImageURL string `json:"imageUrl,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// source
	Source int64 `json:"source,omitempty"`

	// tags
	Tags []string `json:"tags"`

	// url
	URL string `json:"url,omitempty"`
}

func DomainLinkToLinkPresentation(link *entities.Link) *LinkModel {

	//tagsList :=
	tagsList := []string{}
	for _, tag := range link.Tags {
		tagsList = append(tagsList, string(tag))
	}

	modelLink := &LinkModel{
		ID:         string(link.ID),
		ExternalID: link.ExternalID,
		Added:      link.Added,
		Name:       link.Name,
		Source:     int64(link.Source),
		Tags:       tagsList,
		URL:        link.Url,
		Excerpt:    link.Excerpt,
		ImageURL:   link.ImageUrl,
	}

	return modelLink
}
