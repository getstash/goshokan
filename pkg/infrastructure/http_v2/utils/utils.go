package utils

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/schema"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"strings"
)

var (
	ErrEmptyAuthHeader    = errors.New("auth header is empty")
	ErrInvalidAuthHeader  = errors.New("auth header is invalid")
	ErrInvalidCredentials = errors.New("credentials are invalid")
)

const (
	TokenHeaderName   = "Authorization"
	TokenHeadName     = "Bearer"
	BasicAuthHeadName = "Basic"
)

func InternalServerErrorResponse(c *gin.Context) {
	c.JSON(http.StatusInternalServerError, gin.H{})
}

func UnauthorizedResponse(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, gin.H{})
}

func BadRequestResponseFromSchemaErrors(errMap schema.MultiError, c *gin.Context) {
	response := gin.H{}
	for fieldName, err := range errMap {
		switch castedErr := err.(type) {
		case schema.ConversionError:
			response[fieldName] = fmt.Sprintf("cannot convert field. Must be %s", castedErr.Type)
		case schema.EmptyFieldError:
			response[fieldName] = "required field"
		default:
			response[fieldName] = "unsupported error"
		}
	}

	c.JSON(http.StatusBadRequest, response)
}

func UnprocessableEntityResponseForList(c *gin.Context, errors validator.ValidationErrors, itemIndex int) {
	errorsInfo := gin.H{}
	for _, error := range errors {
		errorsInfo[error.Field()] = error.ActualTag()
	}

	c.JSON(http.StatusUnprocessableEntity, gin.H{
		"error": "UnprocessableEntity",
		"index": itemIndex,
		"details": errorsInfo,
	})
}

func UnprocessableEntityResponse(c *gin.Context, errors validator.ValidationErrors) {
	errorsInfo := gin.H{}
	for _, error := range errors {
		errorsInfo[error.Field()] = error.ActualTag()
	}

	c.JSON(http.StatusUnprocessableEntity, gin.H{
		"error": "UnprocessableEntity",
		"details": errorsInfo,
	})
}

func OkResponse(c *gin.Context) {
	c.Status(http.StatusOK)
}

func CreatedResponse(c *gin.Context) {
	c.Status(http.StatusCreated)
}

func GetTokenFromHeader(c *gin.Context) (string, error) {
	authHeader := c.Request.Header.Get(TokenHeaderName)
	if authHeader == "" {
		return "", ErrEmptyAuthHeader
	}
	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == TokenHeadName) {
		return "", ErrInvalidAuthHeader
	}
	return parts[1], nil
}

func GetLoginAndPasswordFromBasicAuth(c *gin.Context) (string, string, error) {
	authHeader := c.Request.Header.Get(TokenHeaderName)
	if authHeader == "" {
		return "", "", ErrEmptyAuthHeader
	}

	parts := strings.SplitN(authHeader, " ", 2)
	if !(len(parts) == 2 && parts[0] == BasicAuthHeadName) {
		return "", "", ErrInvalidAuthHeader
	}

	payload, _ := base64.StdEncoding.DecodeString(parts[1])
	pair := strings.SplitN(string(payload), ":", 2)
	if len(pair) != 2 {
		return "", "", ErrInvalidCredentials
	}

	return pair[0], pair[1], nil
}
