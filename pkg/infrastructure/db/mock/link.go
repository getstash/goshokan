package mock

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

type LinkDAO struct {
}

func (dao *LinkDAO) Upsert([]*entities.Link) error {
	return nil
}

func (dao *LinkDAO) Find(query *interfaces.FindLinkQuery) (*interfaces.FindLinkResponse, error) {
	response := interfaces.FindLinkResponse{}
	response.Links = []*entities.Link{}
	response.Pagination = query.Pagination
	return &response, nil
}

func (dao *LinkDAO) Count(query *interfaces.FindLinkQuery) (int, error) {
	return 0, nil
}
