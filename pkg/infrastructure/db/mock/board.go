package mock

import "goshokan/pkg/domain/entities"

type BoardDAO struct{}

func (BoardDAO) InsertOne(*entities.Board) error {
	return nil
}
