package boltdbdao

import (
	"encoding/json"
	"fmt"
	"github.com/coreos/bbolt"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

var BUCKET_NAME = []byte("board")

type BoardDAO struct {
	db *bolt.DB
}

func (d *BoardDAO) GetById(key string) (*entities.Board, error) {

	var result boardModel

	err := d.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(BUCKET_NAME)
		if bucket == nil {
			return fmt.Errorf("Bucket %q not found!", string(BUCKET_NAME))
		}

		val := bucket.Get([]byte(key))

		if val == nil {
			return interfaces.ErrNoBoard
		}

		err := json.Unmarshal(val, &result)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	entity := entityFromBoardModel(&result)

	return entity, nil
}

func (d *BoardDAO) DeleteById(key string) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(BUCKET_NAME)
		if err != nil {
			return err
		}

		return b.Delete([]byte(key))
	})
}

func (d *BoardDAO) UpsertOne(board *entities.Board) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(BUCKET_NAME)
		if err != nil {
			return err
		}

		boardModel := boardModelFromEntity(board)
		encoded, err := json.Marshal(boardModel)
		if err != nil {
			return err
		}

		return b.Put([]byte(boardModel.ID), encoded)
	})
}

func (d *BoardDAO) FindAll() ([]*entities.Board, error) {
	boards := []*entities.Board{}

	viewErr := d.db.View(func(tx *bolt.Tx) error {
		cursor := tx.Bucket(BUCKET_NAME).Cursor()

		for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
			var result boardModel

			err := json.Unmarshal(v, &result)
			if err != nil {
				return err
			}

			boards = append(boards, entityFromBoardModel(&result))
		}

		return nil
	})
	if viewErr != nil {
		return nil, viewErr
	}

	return boards, nil
}

func (d *BoardDAO) Close() {
	d.db.Close()
}

func NewBoardDAO(path string) (*BoardDAO, error) {
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		return nil, err
	}

	dao := BoardDAO{}
	dao.db = db

	// create bucket
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(BUCKET_NAME)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return &dao, nil
}
