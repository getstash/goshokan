package boltdbdao

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"log"
	"os"
	"testing"
)

type BoltBoardDAOTestSuite struct {
	suite.Suite

	dao *BoardDAO
}

func (suite *BoltBoardDAOTestSuite) SetupTest() {
	dao, err := NewBoardDAO("test.boltdb")
	if err != nil {
		log.Fatal(err)
	}
	suite.dao = dao
}

func (suite *BoltBoardDAOTestSuite) TearDownTest() {
	suite.dao.Close()
	os.RemoveAll("test.boltdb")
}

func (suite *BoltBoardDAOTestSuite) TestGetById_InsertDocument_GetDocument() {
	entity := entities.NewBoard("test_id", "test_name", []entities.LinkID{entities.LinkID("test_link_id")}, false)

	upsertErr := suite.dao.UpsertOne(entity)

	board, getErr := suite.dao.GetById(entity.ID)

	suite.Assert().Nil(upsertErr)
	suite.Assert().Nil(getErr)

	suite.Assert().Equal(entity.ID, board.ID)
	suite.Assert().Equal(entity.Name, board.Name)
	suite.Assert().Equal(entity.IsOpen, board.IsOpen)
	suite.Assert().Equal(entity.LinkIDs, board.LinkIDs)
}

func (suite *BoltBoardDAOTestSuite) TestGetById_NoDocument_CorrectErrorReturned() {

	_, getErr := suite.dao.GetById("TestGetById_NoDocument_CorrectErrorReturned")

	suite.Assert().Equal(interfaces.ErrNoBoard, getErr)

}

func (suite *BoltBoardDAOTestSuite) TestDeleteById_InsertAndDelete_NoDocument() {
	entity := entities.NewBoard("test_id", "test_name", []entities.LinkID{entities.LinkID("test_link_id")}, false)

	upsertErr := suite.dao.UpsertOne(entity)
	board, getErr := suite.dao.GetById(entity.ID)
	deleteErr := suite.dao.DeleteById(board.ID)
	_, secondGetErr := suite.dao.GetById(entity.ID)

	suite.Assert().Nil(upsertErr)
	suite.Assert().Nil(getErr)
	suite.Assert().Nil(deleteErr)
	suite.Assert().NotNil(secondGetErr)

}

func (suite *BoltBoardDAOTestSuite) TestFindAll_Insert10Items_AllReturned() {
	for i := 0; i < 10; i++ {
		entityId := fmt.Sprintf(fmt.Sprintf("test_id_%d", i))
		entity := entities.NewBoard(entityId, "test_name", []entities.LinkID{entities.LinkID("test_link_id")}, false)
		suite.dao.UpsertOne(entity)
	}

	entitiesSlice, findErr := suite.dao.FindAll()

	suite.Assert().Nil(findErr)
	suite.Assert().Equal(10, len(entitiesSlice))
}

func (suite *BoltBoardDAOTestSuite) TestUpsert_InsertOneItem_InsertedWithoutError() {
	entity := entities.NewBoard("test_id", "test_name", []entities.LinkID{entities.LinkID("test_link_id")}, false)

	upsertErr := suite.dao.UpsertOne(entity)

	suite.Assert().Nil(upsertErr)
}

func (suite *BoltBoardDAOTestSuite) TestUpsert_InsertAndUpsertOneItem_InsertedWithoutError() {
	entity := entities.NewBoard("test_id", "test_name", []entities.LinkID{entities.LinkID("test_link_id")}, false)

	upsertErr := suite.dao.UpsertOne(entity)
	upsertSecondErr := suite.dao.UpsertOne(entity)

	suite.Assert().Nil(upsertErr)
	suite.Assert().Nil(upsertSecondErr)
}

func TestBoardDAO(t *testing.T) {
	testSuite := BoltBoardDAOTestSuite{}
	suite.Run(t, &testSuite)
}
