package boltdbdao

import "goshokan/pkg/domain/entities"

type boardModel struct {
	ID      string   `json:"id"`
	Name    string   `json:"name"`
	LinkIDs []string `json:"link_ids"`
	IsOpen  bool     `json:"is_open"`
}

func newBoardModel(id string, name string, linkIds []string, isOpen bool) *boardModel {
	return &boardModel{
		ID:      id,
		Name:    name,
		LinkIDs: linkIds,
		IsOpen:  isOpen,
	}
}

func boardModelFromEntity(entity *entities.Board) *boardModel {
	return newBoardModel(
		entity.ID,
		entity.Name,
		entities.LinkIDListToStringList(entity.LinkIDs),
		entity.IsOpen,
	)
}

func entityFromBoardModel(model *boardModel) *entities.Board {
	return entities.NewBoard(
		model.ID,
		model.Name,
		entities.StringsListToLinkIDList(model.LinkIDs),
		model.IsOpen,
	)
}
