package elastic

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

const AllLinksConst = 1000

type LinkDAO struct {
	client *elastic.Client
}

func (dao *LinkDAO) Upsert(entities []*entities.Link) error {

	bulkRequest := dao.client.Bulk()
	for _, entity := range entities {
		model := linkModelFromEntity(entity)
		req := elastic.NewBulkIndexRequest().Index(linkIndexName).Type(linkTypeName).Id(model.ID).Doc(model)
		bulkRequest = bulkRequest.Add(req)
	}
	_, err := bulkRequest.Do(context.TODO())
	if err != nil {
		return err
	}

	return nil
}

func (dao *LinkDAO) Search(pattern string, limit *int, offset *int, orderBy *interfaces.ILinkDAOOrder) ([]*entities.Link, error) {

	search := dao.client.Search().Index(linkIndexName)

	if pattern != "" {
		mmQuery := elastic.NewMultiMatchQuery(&pattern, "name", "url", "tags", "excerpt")
		search.Query(mmQuery)
	}

	if offset != nil {
		search = search.From(*offset)
	}

	if limit != nil {
		search = search.Size(*limit)
	} else {
		search.Size(AllLinksConst)
	}

	if orderBy != nil {
		if *orderBy == interfaces.OrderByAddedDescending {
			search = search.Sort("added", false)
		} else {
			return nil, interfaces.ErrInvalidOrderConstant
		}
	}

	result, err := search.Do(context.TODO())
	if err != nil {
		return nil, err
	}

	entities := []*entities.Link{}

	for _, hit := range result.Hits.Hits {
		var model linkModel
		err := json.Unmarshal(*hit.Source, &model)
		if err != nil {
			return nil, err
		}
		entities = append(entities, entityFromLinkModel(&model))
	}
	return entities, nil
}

func (dao *LinkDAO) FindByIds(ids []entities.LinkID) ([]*entities.Link, error) {

	pStringIds := []interface{}{}
	for _, entityId := range ids {
		stringId := string(entityId)
		pStringIds = append(pStringIds, &stringId)
	}

	termQuery := elastic.NewTermsQuery("id", pStringIds...)
	search := dao.client.Search().Index(linkIndexName).Query(termQuery).Size(AllLinksConst)
	result, err := search.Do(context.TODO())
	if err != nil {
		return nil, err
	}

	entities := []*entities.Link{}

	for _, hit := range result.Hits.Hits {
		var model linkModel
		err := json.Unmarshal(*hit.Source, &model)
		if err != nil {
			return nil, err
		}
		entities = append(entities, entityFromLinkModel(&model))
	}

	return entities, nil
}

func NewLinkDAO(client *elastic.Client) (*LinkDAO, error) {
	ctx := context.Background()
	exists, err := client.IndexExists(linkIndexName).Do(ctx)
	if err != nil {
		return nil, err
	}

	if !exists {
		_, err = client.CreateIndex(linkIndexName).Body(linkModelMapping).Do(ctx)
		if err != nil {
			return nil, err
		}
	}

	dao := &LinkDAO{}
	dao.client = client

	return dao, nil
}
