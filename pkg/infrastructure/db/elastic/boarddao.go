package elastic

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

type BoardDAO struct {
	client *elastic.Client
}

func (dao *BoardDAO) UpsertOne(entity *entities.Board) error {

	model := boardModelFromEntity(entity)
	req := dao.client.Update().Index(boardIndexName).Type(boardTypeName).Id(model.ID).Doc(model).Upsert(model).Refresh("true")

	_, err := req.Do(context.TODO())
	if err != nil {
		return err
	}

	return nil
}

func (dao *BoardDAO) FindAll() ([]*entities.Board, error) {
	search := dao.client.Search().Index(boardIndexName)
	result, err := search.Do(context.TODO())
	if err != nil {
		return nil, err
	}

	entities := []*entities.Board{}

	for _, hit := range result.Hits.Hits {
		var model boardModel
		err := json.Unmarshal(*hit.Source, &model)
		if err != nil {
			return nil, err
		}
		entities = append(entities, entityFromBoardModel(&model))
	}

	return entities, nil
}

func (dao *BoardDAO) GetById(id string) (*entities.Board, error) {
	pStringIds := []interface{}{}
	pStringIds = append(pStringIds, &id)

	termQuery := elastic.NewTermsQuery("id", pStringIds...)
	search := dao.client.Search().Index(boardIndexName).Query(termQuery)
	result, err := search.Do(context.TODO())
	if err != nil {
		return nil, err
	}

	if len(result.Hits.Hits) < 1 {
		return nil, nil
	}

	var model boardModel
	err = json.Unmarshal(*result.Hits.Hits[0].Source, &model)
	if err != nil {
		return nil, err
	}

	return entityFromBoardModel(&model), nil
}

func (dao *BoardDAO) DeleteById(id string) error {

	_, err := dao.client.Delete().Index(boardIndexName).Type("board").Id(id).Refresh("true").Do(context.TODO())
	if err != nil {
		elasticErr, ok := err.(*elastic.Error)
		if !ok {
			return err
		}

		if elasticErr.Status == 404 {
			return interfaces.ErrNoBoard
		}

		return err
	}

	return nil
}

func NewBoardDAO(client *elastic.Client) (*BoardDAO, error) {
	ctx := context.Background()
	exists, err := client.IndexExists(boardIndexName).Do(ctx)
	if err != nil {
		return nil, err
	}

	if !exists {
		_, err = client.CreateIndex(boardIndexName).Body(boardModelMapping).Do(ctx)
		if err != nil {
			return nil, err
		}
	}

	dao := &BoardDAO{}
	dao.client = client

	return dao, nil
}
