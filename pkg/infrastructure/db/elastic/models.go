package elastic

import (
	"goshokan/pkg/domain/entities"
	"time"
)

const linkIndexName = "links"
const linkTypeName = "link"
const linkModelMapping = `
{
	"settings":{
		"number_of_shards":1,
		"number_of_replicas":0
	},
	"mappings":{
		"link":{
			"properties":{
				"id":{"type":"keyword"},
				"source":{"type":"short"},
				"added":{"type":"date"},
				"external_id":{"type":"keyword"},
				"url":{"type":"text"},
				"name":{"type":"text"},
				"excerpt":{"type":"text"},
				"image_url":{"type":"text"},
				"tags":{"type":"text"}
			}
		}
	}
}
`
const boardIndexName = "boards"
const boardTypeName = "board"
const boardModelMapping = `{
	"settings":{
		"number_of_shards":1,
		"number_of_replicas":0
	},
	"mappings":{
		"board":{
			"properties":{
				"id":{"type":"keyword"},
				"name":{"type":"text"},
				"is_open":{"type":"boolean"},
				"link_ids":{"type":"text"}
			}
		}
	}
}`

type linkModel struct {
	ID     string    `json:"id"`
	Source int       `json:"source"`
	Added  time.Time `json:"added"`

	ExternalID string   `json:"external_id"`
	Url        string   `json:"url"`
	Name       string   `json:"name"`
	Tags       []string `json:"tags"`
	Excerpt    string   `json:"excerpt"`
	ImageUrl   string   `json:"image_url"`
}

func NewLinkModel(id string, source int, added time.Time, externalId string, url string, name string, tags []string, imageUrl string, excerpt string) *linkModel {
	model := linkModel{
		ID:     id,
		Source: source,
		Added:  added,

		ExternalID: externalId,
		Url:        url,
		Name:       name,
		Tags:       tags,
		ImageUrl:   imageUrl,
		Excerpt:    excerpt,
	}

	return &model
}

func linkModelFromEntity(entity *entities.Link) *linkModel {
	return NewLinkModel(
		string(entity.ID),
		int(entity.Source),
		entity.Added,
		entity.ExternalID,
		entity.Url,
		entity.Name,
		entity.Tags,
		entity.ImageUrl,
		entity.Excerpt,
	)
}

func entityFromLinkModel(model *linkModel) *entities.Link {
	return entities.NewLink(
		entities.LinkID(model.ID),
		model.ExternalID,
		model.Url,
		model.Name,
		model.Tags,
		entities.Source(model.Source),
		model.Added,
		model.ImageUrl,
		model.Excerpt,
	)
}

type boardModel struct {
	ID      string   `json:"id"`
	Name    string   `json:"name"`
	LinkIDs []string `json:"link_ids"`
	IsOpen  bool     `json:"is_open"`
}

func NewBoardModel(id string, name string, linkIds []string, isOpen bool) *boardModel {
	return &boardModel{
		ID:      id,
		Name:    name,
		LinkIDs: linkIds,
		IsOpen:  isOpen,
	}
}

func boardModelFromEntity(entity *entities.Board) *boardModel {
	return NewBoardModel(
		entity.ID,
		entity.Name,
		entities.LinkIDListToStringList(entity.LinkIDs),
		entity.IsOpen,
	)
}

func entityFromBoardModel(model *boardModel) *entities.Board {
	return entities.NewBoard(
		model.ID,
		model.Name,
		entities.StringsListToLinkIDList(model.LinkIDs),
		model.IsOpen,
	)
}
