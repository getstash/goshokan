package blevedao

import (
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/analysis/lang/ru"
	"github.com/blevesearch/bleve/mapping"
	"github.com/blevesearch/bleve/search"
	"github.com/blevesearch/bleve/search/query"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"time"
)

const AllLinksConst = 1000

type LinkDAO struct {
	index bleve.Index
}

func (d *LinkDAO) prepareMapping() mapping.IndexMapping {
	ruFieldMapping := bleve.NewTextFieldMapping()
	ruFieldMapping.Analyzer = ru.AnalyzerName

	linkDocumentMapping := bleve.NewDocumentMapping()
	linkDocumentMapping.AddFieldMappingsAt("id", bleve.NewTextFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("source", bleve.NewNumericFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("added", bleve.NewDateTimeFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("external_id", bleve.NewTextFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("url", bleve.NewTextFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("image_url", bleve.NewTextFieldMapping())
	linkDocumentMapping.AddFieldMappingsAt("name", ruFieldMapping)
	linkDocumentMapping.AddFieldMappingsAt("excerpt", ruFieldMapping)

	mapping := bleve.NewIndexMapping()
	mapping.DefaultMapping = linkDocumentMapping
	mapping.DefaultAnalyzer = ru.AnalyzerName

	return mapping
}

func (d *LinkDAO) buildEntity(resultItem *search.DocumentMatch) *entities.Link {
	linkId := entities.LinkID(resultItem.Fields["id"].(string))
	externalId := resultItem.Fields["external_id"].(string)
	url := resultItem.Fields["url"].(string)
	imageUrl := resultItem.Fields["image_url"].(string)
	name := resultItem.Fields["name"].(string)
	excerpt := resultItem.Fields["excerpt"].(string)
	added := resultItem.Fields["added"].(string)
	source := entities.Source(resultItem.Fields["source"].(float64))

	tags := []string{}
	tagsRaw, ok := resultItem.Fields["tags"].([]interface{})
	if ok {
		for _, tag := range tagsRaw {
			tags = append(tags, tag.(string))
		}
	}

	timeAdded, _ := time.Parse(time.RFC3339Nano, added)

	return entities.NewLink(
		linkId,
		externalId,
		url,
		name,
		tags,
		source,
		timeAdded,
		imageUrl,
		excerpt,
	)
}

func (d *LinkDAO) executeSearchRequest(q query.Query, limit *int, offset *int, order *interfaces.ILinkDAOOrder) ([]*entities.Link, error) {
	searchRequest := bleve.NewSearchRequest(q)
	searchRequest.Fields = []string{"id", "external_id", "url", "image_url", "name", "excerpt", "source", "added", "tags"}

	if limit != nil {
		searchRequest.Size = *limit
	}

	if offset != nil {
		searchRequest.From = *offset
	}

	if order != nil && *order == interfaces.OrderByAddedDescending {
		searchRequest.SortBy([]string{"-added"})
	}

	searchResult, err := d.index.Search(searchRequest)
	if err != nil {
		return nil, err
	}

	entities := []*entities.Link{}
	for _, item := range searchResult.Hits {
		entity := d.buildEntity(item)
		entities = append(entities, entity)
	}

	return entities, nil
}

func (d *LinkDAO) Upsert(entitiesList []*entities.Link) error {

	batch := d.index.NewBatch()
	for _, entity := range entitiesList {
		model := linkModelFromEntity(entity)
		batch.Index(model.ID, model)
	}

	return d.index.Batch(batch)
}

func (d *LinkDAO) FindByIds(entityIds []entities.LinkID) ([]*entities.Link, error) {

	ids := entities.LinkIDListToStringList(entityIds)
	allLinks := AllLinksConst

	q := bleve.NewDocIDQuery(ids)

	return d.executeSearchRequest(q, &allLinks, nil, nil)
}

func (d *LinkDAO) Search(pattern string, limit *int, offset *int, order *interfaces.ILinkDAOOrder) ([]*entities.Link, error) {
	var q query.Query

	if pattern == "" {
		q = bleve.NewMatchAllQuery()
	} else {
		mq := bleve.NewMatchPhraseQuery(pattern)
		//tq := bleve.NewTermQuery(pattern)
		//rq := bleve.NewRegexpQuery(pattern)
		//qsq := bleve.NewQueryStringQuery(pattern)
		fq := bleve.NewFuzzyQuery(pattern)
		//q := bleve.NewDisjunctionQuery(mq, rq, qsq, tq, fq)
		q = bleve.NewDisjunctionQuery(mq, fq)
	}

	return d.executeSearchRequest(q, limit, offset, order)
}

func (d *LinkDAO) Close() {
	d.index.Close()
}

func NewLinkDAO(path string) (*LinkDAO, error) {
	dao := &LinkDAO{}

	index, err := bleve.Open(path)
	if err == bleve.ErrorIndexPathDoesNotExist || err == bleve.ErrorIndexMetaMissing {
		mapping := dao.prepareMapping()
		index, err = bleve.New(path, mapping)
		if err != nil {
			return nil, err
		}
	}
	if err != nil {
		return nil, err
	}

	dao.index = index
	return dao, nil
}
