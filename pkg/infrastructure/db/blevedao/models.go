package blevedao

import (
	"goshokan/pkg/domain/entities"
	"time"
)

type linkModel struct {
	ID     string    `json:"id"`
	Source int       `json:"source"`
	Added  time.Time `json:"added"`

	ExternalID string   `json:"external_id"`
	Url        string   `json:"url"`
	Name       string   `json:"name"`
	Tags       []string `json:"tags"`
	Excerpt    string   `json:"excerpt"`
	ImageUrl   string   `json:"image_url"`
}

func NewLinkModel(id string, source int, added time.Time, externalId string, url string, name string, tags []string, imageUrl string, excerpt string) *linkModel {
	model := linkModel{
		ID:     id,
		Source: source,
		Added:  added,

		ExternalID: externalId,
		Url:        url,
		Name:       name,
		Tags:       tags,
		ImageUrl:   imageUrl,
		Excerpt:    excerpt,
	}

	return &model
}

func linkModelFromEntity(entity *entities.Link) *linkModel {
	return NewLinkModel(
		string(entity.ID),
		int(entity.Source),
		entity.Added,
		entity.ExternalID,
		entity.Url,
		entity.Name,
		entity.Tags,
		entity.ImageUrl,
		entity.Excerpt,
	)
}
