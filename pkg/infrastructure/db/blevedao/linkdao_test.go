package blevedao

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"log"
	"os"
	"testing"
	"time"
)

type BleveLinkDAOTestSuite struct {
	suite.Suite

	dao *LinkDAO
}

func (suite *BleveLinkDAOTestSuite) SetupTest() {
	dao, err := NewLinkDAO("test.bleve")
	if err != nil {
		log.Fatal(err)
	}
	suite.dao = dao
}

func (suite *BleveLinkDAOTestSuite) TearDownTest() {
	suite.dao.Close()
	os.RemoveAll("test.bleve")
}

func (suite *BleveLinkDAOTestSuite) prepareEntity() *entities.Link {
	return entities.NewLink(
		entities.LinkID("test_id"),
		"test_external_id",
		"test_url",
		"test_name",
		[]string{"ololo", "ubuntu"},
		entities.SourceInternal,
		time.Now(),
		"test_imageUrl",
		"test_excerpt",
	)
}

func (suite *BleveLinkDAOTestSuite) TestUpsert() {
	entity := suite.prepareEntity()

	suite.dao.Upsert([]*entities.Link{entity})
}

func (suite *BleveLinkDAOTestSuite) TestSearch_LinkAdded_LinkFounded() {
	entity := suite.prepareEntity()

	suite.dao.Upsert([]*entities.Link{entity})

	links, err := suite.dao.Search("test_id", nil, nil, nil)

	suite.Assert().Nil(err)

	loc, _ := time.LoadLocation("Europe/Minsk")

	suite.Assert().Equal(links[0].ID, entity.ID)
	suite.Assert().Equal(links[0].Source, entity.Source)
	suite.Assert().Equal(links[0].Added.In(loc).Format(time.RFC3339), entity.Added.In(loc).Format(time.RFC3339))
	suite.Assert().Equal(links[0].ExternalID, entity.ExternalID)
	suite.Assert().Equal(links[0].ImageUrl, entity.ImageUrl)
	suite.Assert().Equal(links[0].Url, entity.Url)
	suite.Assert().Equal(links[0].Excerpt, entity.Excerpt)
	suite.Assert().Equal(links[0].Name, entity.Name)
	suite.Assert().Equal(links[0].Tags, entity.Tags)
}

func (suite *BleveLinkDAOTestSuite) TestSearch_MinimalLinkAdded_MinimalLinkFounded() {
	entity := suite.prepareEntity()
	entity.Tags = []string{}

	suite.dao.Upsert([]*entities.Link{entity})

	links, err := suite.dao.Search("test_id", nil, nil, nil)

	suite.Assert().Nil(err)

	loc, _ := time.LoadLocation("Europe/Minsk")

	suite.Assert().Equal(links[0].ID, entity.ID)
	suite.Assert().Equal(links[0].Source, entity.Source)
	suite.Assert().Equal(links[0].Added.In(loc).Format(time.RFC3339), entity.Added.In(loc).Format(time.RFC3339))
	suite.Assert().Equal(links[0].ExternalID, entity.ExternalID)
	suite.Assert().Equal(links[0].ImageUrl, entity.ImageUrl)
	suite.Assert().Equal(links[0].Url, entity.Url)
	suite.Assert().Equal(links[0].Excerpt, entity.Excerpt)
	suite.Assert().Equal(links[0].Name, entity.Name)
	suite.Assert().Equal(links[0].Tags, entity.Tags)
}

func (suite *BleveLinkDAOTestSuite) TestSearch_11LinkAddedAndLimitSet_LimitWorks() {
	entity := suite.prepareEntity()

	for i := 0; i < 11; i++ {
		entity.ID = entities.LinkID(fmt.Sprintf("itemId_%d", i))
		suite.dao.Upsert([]*entities.Link{entity})
	}
	limit := 10
	links, err := suite.dao.Search("test_name", &limit, nil, nil)

	suite.Assert().Nil(err)
	suite.Assert().Equal(10, len(links))
}

func (suite *BleveLinkDAOTestSuite) TestSearch_11LinkAddedAndLimitSetOffsetSet_OffsetWorks() {
	entity := suite.prepareEntity()

	for i := 0; i < 11; i++ {
		entity.ID = entities.LinkID(fmt.Sprintf("itemId_%d", i))
		suite.dao.Upsert([]*entities.Link{entity})
	}
	limit := 10
	offset := 10
	links, err := suite.dao.Search("test_name", &limit, &offset, nil)

	suite.Assert().Nil(err)
	suite.Assert().Equal(1, len(links))
}

func (suite *BleveLinkDAOTestSuite) TestSearch_5LinkAddedAndNoSearchTermProvided_ReturnAllData() {
	entity := suite.prepareEntity()

	for i := 0; i < 5; i++ {
		entity.ID = entities.LinkID(fmt.Sprintf("itemId_%d", i))
		suite.dao.Upsert([]*entities.Link{entity})
	}
	links, err := suite.dao.Search("", nil, nil, nil)

	suite.Assert().Nil(err)
	suite.Assert().Equal(5, len(links))
}

func (suite *BleveLinkDAOTestSuite) TestSearch_OrderByAddedDescendingAdded_LinksSortedCorrectly() {
	entityOne := suite.prepareEntity()
	entityTwo := suite.prepareEntity()

	entityOne.ID = entities.LinkID("ONE")
	entityOne.Added = time.Now().Add(-1 * time.Duration(10*time.Hour))

	entityTwo.ID = entities.LinkID("TWO")

	suite.dao.Upsert([]*entities.Link{entityOne, entityTwo})

	orderByAdded := interfaces.OrderByAddedDescending

	links, err := suite.dao.Search("test_name", nil, nil, &orderByAdded)

	suite.Assert().Nil(err)
	suite.Assert().True(entityTwo.ID == links[0].ID)
}

func (suite *BleveLinkDAOTestSuite) TestSearch_NotExactEnglishWord_ResultFinded() {
	entity := suite.prepareEntity()
	entity.Name = "Girls"

	suite.dao.Upsert([]*entities.Link{entity})

	links, err := suite.dao.Search("girl", nil, nil, nil)

	suite.Assert().Nil(err)
	suite.Assert().Equal(entity.ID, links[0].ID)
}

func (suite *BleveLinkDAOTestSuite) TestFindByIds_IdExists_ResultFinded() {
	entity := suite.prepareEntity()

	suite.dao.Upsert([]*entities.Link{entity})

	links, err := suite.dao.FindByIds([]entities.LinkID{entity.ID})

	suite.Assert().Nil(err)
	suite.Assert().Equal(entity.ID, links[0].ID)
}

func TestLinkDAO(t *testing.T) {
	testSuite := BleveLinkDAOTestSuite{}
	suite.Run(t, &testSuite)
}
