package mongo

import (
	"goshokan/pkg/domain/entities"
	"time"
)

type linkModel struct {
	ID     string    `bson:"_id"`
	Source int       `bson:"source"`
	Added  time.Time `bson:"added"`

	ExternalID string   `bson:"external_id"`
	Url        string   `bson:"url"`
	Name       string   `bson:"name"`
	Tags       []string `bson:"tags"`
	Excerpt    string   `bson:"excerpt"`
	ImageUrl   string   `bson:"image_url"`
}

func NewLinkModel(id string, source int, added time.Time, externalId string, url string, name string, tags []string, imageUrl string, excerpt string) *linkModel {
	model := linkModel{
		ID:     id,
		Source: source,
		Added:  added,

		ExternalID: externalId,
		Url:        url,
		Name:       name,
		Tags:       tags,
		ImageUrl:   imageUrl,
		Excerpt:    excerpt,
	}

	return &model
}

func linkModelFromEntity(entity *entities.Link) *linkModel {
	return NewLinkModel(
		string(entity.ID),
		int(entity.Source),
		entity.Added,
		entity.ExternalID,
		entity.Url,
		entity.Name,
		entity.Tags,
		entity.ImageUrl,
		entity.Excerpt,
	)
}

func entityFromLinkModel(model *linkModel) *entities.Link {
	return entities.NewLink(
		entities.LinkID(model.ID),
		model.ExternalID,
		model.Url,
		model.Name,
		model.Tags,
		entities.Source(model.Source),
		model.Added,
		model.ImageUrl,
		model.Excerpt,
	)
}

type boardModel struct {
	ID      string   `bson:"_id"`
	Name    string   `bson:"name"`
	LinkIDs []string `bson:"link_ids"`
	IsOpen  bool     `bson:"is_open"`
}

func NewBoardModel(id string, name string, linkIds []string, isOpen bool) *boardModel {
	return &boardModel{
		ID:      id,
		Name:    name,
		LinkIDs: linkIds,
		IsOpen:  isOpen,
	}
}

func boardModelFromEntity(entity *entities.Board) *boardModel {
	return NewBoardModel(
		entity.ID,
		entity.Name,
		entities.LinkIDListToStringList(entity.LinkIDs),
		entity.IsOpen,
	)
}

func entityFromBoardModel(model *boardModel) *entities.Board {
	return entities.NewBoard(
		model.ID,
		model.Name,
		entities.StringsListToLinkIDList(model.LinkIDs),
		model.IsOpen,
	)
}
