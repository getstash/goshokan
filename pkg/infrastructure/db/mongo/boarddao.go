package mongo

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"goshokan/pkg/domain/entities"
)

type BoardDAO struct {
	baseDAO
}

func (dao *BoardDAO) UpsertOne(entity *entities.Board) error {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)
	model := boardModelFromEntity(entity)

	_, err := collection.UpsertId(model.ID, model)
	return err
}

func (dao *BoardDAO) FindAll() ([]*entities.Board, error) {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)

	models := []*boardModel{}
	entitiesList := []*entities.Board{}

	err := collection.Find(bson.M{}).All(&models)
	if err != nil {
		return nil, err
	}

	for _, model := range models {
		entitiesList = append(entitiesList, entityFromBoardModel(model))
	}

	return entitiesList, nil
}

func (dao *BoardDAO) GetById(id string) (*entities.Board, error) {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)

	model := &boardModel{}
	err := collection.FindId(id).One(&model)
	if err != nil {
		return nil, err
	}

	return entityFromBoardModel(model), nil
}

func NewBoardDAO(session *mgo.Session, dbName string, collection string) (*BoardDAO, error) {
	return &BoardDAO{
		baseDAO{
			session:    session,
			dbName:     dbName,
			collection: collection,
		},
	}, nil
}
