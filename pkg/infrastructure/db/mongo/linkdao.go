package mongo

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

type LinkDAO struct {
	baseDAO
}

func (dao *LinkDAO) Upsert(entities []*entities.Link) error {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)
	bulk := collection.Bulk()
	bulk.Upsert()
	for _, link := range entities {

		model := linkModelFromEntity(link)

		update := bson.M{"$set": model}
		selector := bson.M{"_id": model.ID}

		bulk.Upsert(selector, update)
	}
	_, err := bulk.Run()
	return err
}

func (dao *LinkDAO) Find(query *interfaces.FindLinkQuery) (*interfaces.FindLinkResponse, error) {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)
	q, err := dao.buildMgoQueryObjFromQuery(collection, query)
	if err != nil {
		return nil, err
	}

	q = dao.applyMeta(q, &query.Pagination)

	var models []*linkModel

	err = q.All(&models)
	if err != nil {
		return nil, err
	}

	var entities []*entities.Link

	for _, model := range models {
		entities = append(entities, entityFromLinkModel(model))
	}

	response := interfaces.FindLinkResponse{}
	response.Links = entities
	response.Pagination = query.Pagination
	return &response, nil
}

func (dao *LinkDAO) buildMgoQueryObjFromQuery(c *mgo.Collection, query *interfaces.FindLinkQuery) (*mgo.Query, error) {
	q := c.Find(dao.getFindArgs(query))
	return q, nil
}

func (dao *LinkDAO) getFindArgs(query *interfaces.FindLinkQuery) bson.M {

	filter := bson.M{}

	if query.Filter.Tag != nil {
		filter["tags"] = *query.Filter.Tag
	}

	return filter
}

func (dao *LinkDAO) applyMeta(q *mgo.Query, pagination *interfaces.Pagination) *mgo.Query {
	q = q.Limit(pagination.Limit)
	q = q.Skip(pagination.Offset)
	return q
}

func (dao *LinkDAO) Count(query *interfaces.FindLinkQuery) (int, error) {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)
	q, err := dao.buildMgoQueryObjFromQuery(collection, query)
	if err != nil {
		return 0, err
	}

	count, err := q.Count()
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (dao *LinkDAO) FindByIds(ids []entities.LinkID) ([]*entities.Link, error) {
	session := dao.getSessionWithClone()
	defer session.Close()

	collection := dao.getCollection(session)

	strIds := entities.LinkIDListToStringList(ids)

	query := bson.M{
		"_id": bson.M{
			"$in": strIds,
		},
	}

	models := []*linkModel{}

	err := collection.Find(query).All(&models)
	if err != nil {
		return nil, err
	}

	entitiesList := []*entities.Link{}
	for _, model := range models {
		entitiesList = append(entitiesList, entityFromLinkModel(model))
	}

	return entitiesList, nil
}

func NewLinkDAO(session *mgo.Session, dbName string, collection string) (*LinkDAO, error) {
	return &LinkDAO{
		baseDAO{
			session:    session,
			dbName:     dbName,
			collection: collection,
		},
	}, nil
}
