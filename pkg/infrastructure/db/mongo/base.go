package mongo

import (
	"github.com/globalsign/mgo"
)

type baseDAO struct {
	session *mgo.Session

	dbName     string
	collection string
}

func (dao *baseDAO) getSessionWithClone() *mgo.Session {
	return dao.session.Clone()
}

func (dao *baseDAO) getCollection(session *mgo.Session) *mgo.Collection {
	return session.DB(dao.dbName).C(dao.collection)
}

func (dao *baseDAO) Drop() error {
	session := dao.getSessionWithClone()
	defer session.Close()

	return dao.getCollection(session).DropCollection()
}
