package mock

type IDService struct{}

func (s *IDService) Generate() (string, error) {
	// This is a really random ID
	return "4", nil
}
