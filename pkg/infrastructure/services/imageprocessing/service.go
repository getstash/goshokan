package imageprocessing

import (
	"bytes"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
)

type Service struct {
	fetchService *FetchService
}

func (s *Service) FetchAndGetSize(url string) (int, int, bool) {
	content, fetchErr := s.fetchService.GetBodyFromUrl(url)
	if fetchErr != nil {
		return 0, 0, false
	}

	reader := bytes.NewReader(content)
	config, _, decodeErr := image.DecodeConfig(reader)
	if decodeErr != nil {
		return 0, 0, false
	}

	return config.Height, config.Width, true
}

func NewService() *Service {
	fetchService := NewFetchService(5000)
	service := Service{
		fetchService: fetchService,
	}
	return &service
}
