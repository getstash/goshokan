package pocket

import (
	"github.com/motemen/go-pocket/api"
	"goshokan/pkg/domain/entities"
	"strconv"
	"time"
)

type PocketService struct {
	accessToken string
	consumerKey string
}

func (s *PocketService) FetchAll() ([]*entities.LinkImportInfo, error) {

	client := api.NewClient(s.consumerKey, s.accessToken)
	results, err := client.Retrieve(&api.RetrieveOption{DetailType: api.DetailTypeComplete})
	if err != nil {
		return nil, err
	}

	var linksInfos []*entities.LinkImportInfo

	for _, item := range results.List {

		var tags []string
		for k := range item.Tags {
			tags = append(tags, k)
		}

		var images []*entities.LinkImage
		for _, value := range item.Images {

			url := value["src"].(string)
			height, _ := strconv.Atoi(value["height"].(string))
			width, _ := strconv.Atoi(value["width"].(string))

			images = append(images, &entities.LinkImage{Url: url, Height: height, Width: width})
		}
		link := &entities.Link{
			Name:       item.ResolvedTitle,
			Url:        item.ResolvedURL,
			Tags:       tags,
			ExternalID: strconv.Itoa(item.ItemID),
			ImageUrl:   "",
			Excerpt:    item.Excerpt,
			Added:      time.Time(item.TimeAdded),
		}

		linkInfo := &entities.LinkImportInfo{
			Link:   link,
			Images: images,
		}

		linksInfos = append(linksInfos, linkInfo)
	}

	return linksInfos, nil
}

func NewPocketService(accessToken, consumerKey string) *PocketService {
	return &PocketService{
		accessToken,
		consumerKey,
	}
}
