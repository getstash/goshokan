package idgeneration

import "github.com/satori/go.uuid"

type UUIDIDGenerationService struct{}

func (s *UUIDIDGenerationService) Generate() string {
	u1 := uuid.NewV4()
	return u1.String()
}
