package logger

import (
	"log"
)

type DefaultLogger struct{}

func (l *DefaultLogger) Info(message string) {
	log.Printf("INFO: %s\n", message)
}

func (l *DefaultLogger) ErrorWithMessage(err error, message string) {
	log.Printf("ERROR: %s; error:%s\n", message, err)
}
