package services

import (
	"github.com/dgrijalva/jwt-go"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"time"
)

type JWTTokenService struct {
	key string
}

func (s *JWTTokenService) ConvertStringToToken(tokenString string) (*entities.Token, error) {

	claims := jwt.StandardClaims{}

	_, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, interfaces.ErrBadToken
		}

		if token.Method != jwt.SigningMethodHS256 {
			return nil, interfaces.ErrBadToken
		}

		return []byte(s.key), nil
	})

	if err != nil {
		return nil, err
	}

	return &entities.Token{IssuedAt: time.Unix(claims.IssuedAt, 0)}, nil
}

func (s *JWTTokenService) ConvertTokenToString(token *entities.Token) (string, error) {
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		IssuedAt: token.IssuedAt.Unix(),
	})
	return jwtToken.SignedString([]byte(s.key))
}

func NewJWTTokenService(key string) *JWTTokenService {
	return &JWTTokenService{key: key}
}
