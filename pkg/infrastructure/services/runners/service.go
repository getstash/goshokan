package runners

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"sync"
)

const defaultGoroutinesValue = 10

type GoroutineTaskFactory struct {
	factory interfaces.SelectImageTaskFactory
}

func (f *GoroutineTaskFactory) FindSuitableImage(infos []*entities.LinkImportInfo) interfaces.IFindSuitableImageTask {
	return NewGoroutineFindSuitableImageTask(infos, f.factory)
}

func NewGoroutineTaskFactory(factory interfaces.SelectImageTaskFactory) *GoroutineTaskFactory {
	return &GoroutineTaskFactory{factory: factory}
}

type GoroutineFindSuitableImageTask struct {
	jobsCh    chan entities.LinkImportInfo
	resultsCh chan entities.LinkImportInfo
	factory   interfaces.SelectImageTaskFactory
	wg        sync.WaitGroup

	count int
}

func (t *GoroutineFindSuitableImageTask) Run() {
	for i := 0; i < t.count; i++ {
		t.wg.Add(1)
		go t.goroutine()
	}
}

func (t *GoroutineFindSuitableImageTask) goroutine() {
	useCase := t.factory()

	for {
		select {
		case info := <-t.jobsCh:
			result := useCase.Execute(&info)
			t.resultsCh <- *result
		default:
			t.wg.Done()
			return
		}
	}
}

func (t *GoroutineFindSuitableImageTask) WaitUntilComplete() {
	t.wg.Wait()
}

func (t *GoroutineFindSuitableImageTask) GetResult() []*entities.LinkImportInfo {
	var results []*entities.LinkImportInfo
	stop := false

	for !stop {
		select {
		case info := <-t.resultsCh:
			results = append(results, &info)
		default:
			stop = true
		}
	}

	return results
}

func NewGoroutineFindSuitableImageTask(infos []*entities.LinkImportInfo, factory interfaces.SelectImageTaskFactory) *GoroutineFindSuitableImageTask {
	jobsCh := make(chan entities.LinkImportInfo, len(infos))
	resultsCh := make(chan entities.LinkImportInfo, len(infos))

	for _, info := range infos {
		jobsCh <- *info
	}

	return &GoroutineFindSuitableImageTask{
		jobsCh:    jobsCh,
		resultsCh: resultsCh,
		factory:   factory,
		count:     defaultGoroutinesValue,
	}
}
