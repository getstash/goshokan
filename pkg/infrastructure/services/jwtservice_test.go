package services

import (
	"goshokan/pkg/domain/entities"
	"testing"
	"time"
)

func TestJWTTokenService_ConvertTokenToStringAndBack(t *testing.T) {
	service := JWTTokenService{"test_key"}
	token := entities.Token{IssuedAt: time.Now()}

	tokenString, err := service.ConvertTokenToString(&token)
	if err != nil {
		panic(err)
	}

	parsedToken, err := service.ConvertStringToToken(tokenString)
	if err != nil {
		panic(err)
	}

	if parsedToken.IssuedAt.Unix() != token.IssuedAt.Unix() {
		panic("invalid generated")
	}
}
