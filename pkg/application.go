package pkg

import (
	"github.com/olivere/elastic"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
	"goshokan/pkg/domain/usecases"
	"goshokan/pkg/infrastructure/config"
	"goshokan/pkg/infrastructure/db/blevedao"
	"goshokan/pkg/infrastructure/db/boltdbdao"
	elasticdb "goshokan/pkg/infrastructure/db/elastic"
	"goshokan/pkg/infrastructure/http"
	"goshokan/pkg/infrastructure/http_v2"
	infraServices "goshokan/pkg/infrastructure/services"
	"goshokan/pkg/infrastructure/services/idgeneration"
	"goshokan/pkg/infrastructure/services/imageprocessing"
	"goshokan/pkg/infrastructure/services/logger"
	"goshokan/pkg/infrastructure/services/pocket"
	"goshokan/pkg/infrastructure/services/runners"
	"os"
	"time"
)

type Goshokan struct {
	linkDAO  interfaces.ILinkDAO
	boardDAO interfaces.IBoardDAO
	config   *config.Config

	pocketService          interfaces.IPocketService
	tokenService           interfaces.ITokenService
	imageProcessingService interfaces.IImageProcessingService

	imageSelectionService             *services.ImageSelectionService
	getBoardWithLinksAggregateService *services.GetBoardWithLinksAggregateService
	authService                       *services.AuthService

	taskFactory interfaces.ITaskFactory

	AddNewBoardUseCase    *usecases.AddNewBoardUseCase
	FindBoardsUseCase     *usecases.FindBoardsUseCase
	GetBoardUseCase       *usecases.GetBoardUseCase
	GetPublicBoardUseCase *usecases.GetBoardUseCase
	DeleteBoardUseCase    *usecases.DeleteBoardUseCase
	UpdateBoardUseCase    *usecases.UpdateBoardUseCase

	SearchLinksUseCase  *usecases.SearchLinksUseCase
	AddNewLinksUseCase  *usecases.AddNewLinksUseCase
	PocketImportUseCase *usecases.PocketImportUseCase

	LoginUseCase *usecases.LoginUseCase

	Logger interfaces.ILogger

	httpService   *http.HTTPService
	httpV2Service *http_v2.HTTPService
}

func (app *Goshokan) initDAO() error {

	if app.config.StorageEngine == config.StorageEngineElasticSearch || app.config.SearchEngine == config.SearchEngineElasticSearch {
		client, err := elastic.NewClient(elastic.SetSniff(false), elastic.SetHealthcheckTimeoutStartup(time.Second*60), elastic.SetURL(app.config.ElasticsearchURI))
		if err != nil {
			return err
		}

		if app.config.SearchEngine == config.SearchEngineElasticSearch {
			linkDAO, err := elasticdb.NewLinkDAO(client)
			if err != nil {
				return err
			}
			app.linkDAO = linkDAO
		}

		if app.config.StorageEngine == config.StorageEngineElasticSearch {
			boardDAO, err := elasticdb.NewBoardDAO(client)
			if err != nil {
				return err
			}
			app.boardDAO = boardDAO
		}
	}

	if app.config.StorageEngine == config.StorageEngineBoltDB {
		boardDAO, err := boltdbdao.NewBoardDAO(app.config.StorageEngineStoragePath)
		if err != nil {
			return err
		}
		app.boardDAO = boardDAO
	}

	if app.config.SearchEngine == config.SearchEngineBleve {
		linkDAO, err := blevedao.NewLinkDAO(app.config.SearchEngineStoragePath)
		if err != nil {
			return err
		}
		app.linkDAO = linkDAO
	}

	return nil
}

func (g *Goshokan) GetHttpService() *http.HTTPService {
	return g.httpService
}

func (g *Goshokan) GetHttpV2Service() *http_v2.HTTPService {
	return g.httpV2Service
}

func (g *Goshokan) GetPocketImportUseCase() *usecases.PocketImportUseCase {
	return g.PocketImportUseCase
}

func (g *Goshokan) GetLogger() interfaces.ILogger {
	return g.Logger
}

func (g *Goshokan) GetSelectImageTaskFactory() interfaces.SelectImageTaskFactory {
	return func() interfaces.ISelectImageUseCase {
		selectionService := services.NewImageSelectionService(imageprocessing.NewService())
		return usecases.NewSelectImageUseCase(selectionService)
	}
}

func NewGoshokan() *Goshokan {

	idService := &idgeneration.UUIDIDGenerationService{}

	goshokan := &Goshokan{}
	goshokan.Logger = &logger.DefaultLogger{}

	config, configReadErr := config.ReadConfig()
	if configReadErr != nil {
		goshokan.Logger.ErrorWithMessage(configReadErr, "ReadConfig. Exiting")
		os.Exit(1)
	}

	goshokan.config = config
	goshokan.imageProcessingService = imageprocessing.NewService()
	goshokan.pocketService = pocket.NewPocketService(config.PocketAccessToken, config.PocketConsumerKey)
	goshokan.tokenService = infraServices.NewJWTTokenService(config.SecretKey)

	err := goshokan.initDAO()
	if err != nil {
		goshokan.Logger.ErrorWithMessage(err, "initDAO. Exiting")
		os.Exit(1)
	}

	goshokan.imageSelectionService = services.NewImageSelectionService(goshokan.imageProcessingService)
	goshokan.getBoardWithLinksAggregateService = services.NewGetBoardWithLinksAggregateService(goshokan.boardDAO, goshokan.linkDAO)
	goshokan.authService = services.NewAuthService(goshokan.tokenService, config.AdminUsername, config.AdminPassword)

	goshokan.taskFactory = runners.NewGoroutineTaskFactory(goshokan.GetSelectImageTaskFactory())

	goshokan.AddNewBoardUseCase = usecases.NewAddNewBoardUseCase(goshokan.boardDAO, idService, goshokan.authService)
	goshokan.FindBoardsUseCase = usecases.NewFindBoardsUseCase(goshokan.boardDAO, goshokan.authService)
	goshokan.GetBoardUseCase = usecases.NewGetBoardUseCase(goshokan.getBoardWithLinksAggregateService, goshokan.authService, false)
	goshokan.GetPublicBoardUseCase = usecases.NewGetBoardUseCase(goshokan.getBoardWithLinksAggregateService, goshokan.authService, true)
	goshokan.DeleteBoardUseCase = usecases.NewDeleteBoardUseCase(goshokan.boardDAO, goshokan.authService)
	goshokan.UpdateBoardUseCase = usecases.NewUpdateBoardUseCase(goshokan.boardDAO, goshokan.getBoardWithLinksAggregateService, goshokan.authService)

	goshokan.AddNewLinksUseCase = usecases.NewAddNewLinksUseCase(goshokan.linkDAO, idService, goshokan.authService)
	goshokan.SearchLinksUseCase = usecases.NewSearchLinksUseCase(goshokan.linkDAO, goshokan.authService)

	goshokan.LoginUseCase = usecases.NewLoginUseCase(goshokan.authService)

	goshokan.PocketImportUseCase = usecases.NewPocketImportUseCase(goshokan.linkDAO, idService, goshokan.pocketService, goshokan.taskFactory, goshokan.Logger)

	goshokan.httpService = http.NewHTTPService(
		goshokan.AddNewLinksUseCase,
		goshokan.AddNewBoardUseCase,
		goshokan.FindBoardsUseCase,
		goshokan.GetBoardUseCase,
		goshokan.SearchLinksUseCase,
		goshokan.DeleteBoardUseCase,
		goshokan.UpdateBoardUseCase,
		goshokan.LoginUseCase,
		config.CookieLifeTimeMs,
		goshokan.Logger,
	)

	goshokan.httpV2Service = http_v2.NewHTTPService(
		goshokan.AddNewLinksUseCase,
		goshokan.AddNewBoardUseCase,
		goshokan.FindBoardsUseCase,
		goshokan.GetBoardUseCase,
		goshokan.GetPublicBoardUseCase,
		goshokan.SearchLinksUseCase,
		goshokan.DeleteBoardUseCase,
		goshokan.UpdateBoardUseCase,
		goshokan.LoginUseCase,
		goshokan.Logger,
		goshokan.config.CORSAllowedOrigin,
	)

	goshokan.Logger.Info("Hurray! Goshokan Initialized and ready to go! ✨🍰✨")
	return goshokan
}
