package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type FindBoardsUseCase struct {
	boardDAO    interfaces.IBoardDAO
	authService *services.AuthService
}

func (u *FindBoardsUseCase) Execute(token string) ([]*entities.Board, error) {

	authErr := u.authService.AuthorizeByToken(token)
	if authErr != nil {
		return nil, authErr
	}

	return u.boardDAO.FindAll()
}

func NewFindBoardsUseCase(boardDAO interfaces.IBoardDAO, authService *services.AuthService) *FindBoardsUseCase {
	return &FindBoardsUseCase{
		boardDAO:    boardDAO,
		authService: authService,
	}
}
