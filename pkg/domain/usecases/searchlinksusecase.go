package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type SearchLinksUseCase struct {
	linkDAO     interfaces.ILinkDAO
	authService *services.AuthService
}

func (u *SearchLinksUseCase) Execute(pattern string, limit, offset *int, token string) ([]*entities.Link, error) {
	authErr := u.authService.AuthorizeByToken(token)
	if authErr != nil {
		return nil, authErr
	}

	orderBy := interfaces.OrderByAddedDescending
	return u.linkDAO.Search(pattern, limit, offset, &orderBy)
}

func NewSearchLinksUseCase(linkDAO interfaces.ILinkDAO, authService *services.AuthService) *SearchLinksUseCase {
	useCase := SearchLinksUseCase{}
	useCase.linkDAO = linkDAO
	useCase.authService = authService
	return &useCase
}
