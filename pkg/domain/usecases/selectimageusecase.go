package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/services"
)

type SelectImageUseCase struct {
	imageSelectionService *services.ImageSelectionService
}

func (u *SelectImageUseCase) Execute(info *entities.LinkImportInfo) *entities.LinkImportInfo {
	suitableImage := u.imageSelectionService.SelectImageFromLinkInfo(info.Images)

	if suitableImage != nil {
		info.Link.ImageUrl = suitableImage.Url
	} else {
		info.Link.ImageUrl = ""
	}

	return info
}

func NewSelectImageUseCase(imageSelectionService *services.ImageSelectionService) *SelectImageUseCase {
	return &SelectImageUseCase{
		imageSelectionService: imageSelectionService,
	}
}
