package usecases

import (
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type LoginUseCase struct {
	authService *services.AuthService
}

func (u *LoginUseCase) Execute(username, password string) (string, error) {
	if !u.authService.IsAuthorizedByBasic(username, password) {
		return "", interfaces.ErrWrongCredentials
	}

	return u.authService.GenerateStringToken()
}

func NewLoginUseCase(service *services.AuthService) *LoginUseCase {
	return &LoginUseCase{authService: service}
}
