package usecases

import (
	"fmt"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

type PocketImportUseCase struct {
	linksDAO    interfaces.ILinkDAO
	idService   interfaces.IIDService
	pocket      interfaces.IPocketService
	logger      interfaces.ILogger
	taskFactory interfaces.ITaskFactory
}

func (u *PocketImportUseCase) Execute() (int, error) {

	u.logger.Info("Start download from pocket")

	linksInfo, err := u.pocket.FetchAll()
	if err != nil {
		return 0, err
	}

	u.logger.Info(fmt.Sprintf("Download from pocket finished 👌. Download %d items", len(linksInfo)))

	for _, linkInfo := range linksInfo {
		u.addIdAndSource(linkInfo)
	}

	newLinksInfo, err := u.findNewLinks(linksInfo)
	if err != nil {
		return 0, err
	}

	u.logger.Info(fmt.Sprintf("Find %d new items", len(newLinksInfo)))

	task := u.taskFactory.FindSuitableImage(newLinksInfo)
	task.Run()
	task.WaitUntilComplete()
	newLinksInfo = task.GetResult()

	u.logger.Info(fmt.Sprintf("Select images for %d new items", len(newLinksInfo)))

	var linksToUpsert []*entities.Link
	for _, newInfo := range newLinksInfo {
		linksToUpsert = append(linksToUpsert, newInfo.Link)
	}

	upsertErr := u.linksDAO.Upsert(linksToUpsert)
	if upsertErr != nil {
		return 0, upsertErr
	}

	u.logger.Info(fmt.Sprintf("Import from Pocket finished 👌. Imported %d items", len(newLinksInfo)))

	return len(newLinksInfo), nil
}

func (u *PocketImportUseCase) addIdAndSource(linkInfo *entities.LinkImportInfo) {
	linkInfo.Link.ID = entities.GenerateLinkID(entities.SourcePocket, linkInfo.Link.ExternalID)
	linkInfo.Link.Source = entities.SourcePocket
}

func (u *PocketImportUseCase) findNewLinks(downloadedLinksInfos []*entities.LinkImportInfo) ([]*entities.LinkImportInfo, error) {

	var newLinksInfos []*entities.LinkImportInfo
	var chunkLinksInfos []*entities.LinkImportInfo
	chunkSize := 250

	for _, link := range downloadedLinksInfos {
		chunkLinksInfos = append(chunkLinksInfos, link)

		if len(chunkLinksInfos) == chunkSize {
			chunkNewLinks, err := u.findNewLinksForChunk(chunkLinksInfos)
			if err != nil {
				return nil, err
			}
			newLinksInfos = append(newLinksInfos, chunkNewLinks...)
			chunkLinksInfos = []*entities.LinkImportInfo{}
		}
	}

	if len(chunkLinksInfos) > 0 {
		chunkNewLinks, err := u.findNewLinksForChunk(chunkLinksInfos)
		if err != nil {
			return nil, err
		}
		newLinksInfos = append(newLinksInfos, chunkNewLinks...)
	}

	return newLinksInfos, nil
}

func (u *PocketImportUseCase) findNewLinksForChunk(downloadedLinks []*entities.LinkImportInfo) ([]*entities.LinkImportInfo, error) {
	var newLinksInfos []*entities.LinkImportInfo
	var linksIds []entities.LinkID
	existingInDBLinksMap := make(map[entities.LinkID]bool)

	for _, link := range downloadedLinks {
		linksIds = append(linksIds, link.Link.ID)
	}

	existingInDBLinks, err := u.linksDAO.FindByIds(linksIds)
	if err != nil {
		return nil, err
	}

	for _, link := range existingInDBLinks {
		existingInDBLinksMap[link.ID] = true
	}

	for _, link := range downloadedLinks {
		_, exists := existingInDBLinksMap[link.Link.ID]
		if !exists {
			newLinksInfos = append(newLinksInfos, link)
		}
	}

	return newLinksInfos, nil
}

func NewPocketImportUseCase(linksDAO interfaces.ILinkDAO, idService interfaces.IIDService, pocketService interfaces.IPocketService, taskFactory interfaces.ITaskFactory, logger interfaces.ILogger) *PocketImportUseCase {
	useCase := &PocketImportUseCase{}
	useCase.linksDAO = linksDAO
	useCase.idService = idService
	useCase.pocket = pocketService
	useCase.logger = logger
	useCase.taskFactory = taskFactory
	return useCase
}
