package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type AddNewBoardUseCase struct {
	boardDAO    interfaces.IBoardDAO
	idService   interfaces.IIDService
	authService *services.AuthService
}

func (u *AddNewBoardUseCase) Execute(req *interfaces.AddNewBoardUseCaseRequest) error {
	authErr := u.authService.AuthorizeByToken(req.Token)
	if authErr != nil {
		return authErr
	}

	board := entities.NewBoard(u.idService.Generate(), req.Name, req.LinkIDs, req.IsOpen)

	return u.boardDAO.UpsertOne(board)
}

func NewAddNewBoardUseCase(boardDAO interfaces.IBoardDAO, idService interfaces.IIDService, authService *services.AuthService) *AddNewBoardUseCase {
	return &AddNewBoardUseCase{
		boardDAO:    boardDAO,
		idService:   idService,
		authService: authService,
	}
}
