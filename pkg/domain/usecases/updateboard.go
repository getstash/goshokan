package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type UpdateBoardUseCase struct {
	boardDAO              interfaces.IBoardDAO
	boardAggregateService *services.GetBoardWithLinksAggregateService
	authService           *services.AuthService
}

func (u *UpdateBoardUseCase) Execute(req *interfaces.UpdateBoardUseCaseRequest) (*entities.BoardWithLinksAggregate, error) {
	authErr := u.authService.AuthorizeByToken(req.Token)
	if authErr != nil {
		return nil, authErr
	}

	board, getByIdErr := u.boardDAO.GetById(req.ID)
	if getByIdErr != nil {
		return nil, getByIdErr
	}

	// If there is no board with such id return an error
	if board == nil {
		return nil, interfaces.ErrNoBoard
	}

	board.IsOpen = req.IsOpen
	board.LinkIDs = req.LinkIDs
	board.Name = req.Name

	upsertOneErr := u.boardDAO.UpsertOne(board)
	if upsertOneErr != nil {
		return nil, upsertOneErr
	}

	return u.boardAggregateService.GetById(req.ID)

}

func NewUpdateBoardUseCase(boardDAO interfaces.IBoardDAO, boardAggregateService *services.GetBoardWithLinksAggregateService, authService *services.AuthService) *UpdateBoardUseCase {
	return &UpdateBoardUseCase{
		boardDAO:              boardDAO,
		boardAggregateService: boardAggregateService,
		authService:           authService,
	}
}
