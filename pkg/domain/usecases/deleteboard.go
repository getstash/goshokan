package usecases

import (
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type DeleteBoardUseCase struct {
	boardDAO    interfaces.IBoardDAO
	authService *services.AuthService
}

func (u *DeleteBoardUseCase) Execute(id string, token string) error {
	err := u.authService.AuthorizeByToken(token)
	if err != nil {
		return err
	}

	return u.boardDAO.DeleteById(id)
}

func NewDeleteBoardUseCase(dao interfaces.IBoardDAO, authService *services.AuthService) *DeleteBoardUseCase {
	return &DeleteBoardUseCase{boardDAO: dao, authService: authService}
}
