package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
	"time"
)

type AddNewLinksUseCase struct {
	linksDAO    interfaces.ILinkDAO
	idService   interfaces.IIDService
	authService *services.AuthService
}

func (u *AddNewLinksUseCase) Execute(req *interfaces.AddNewLinkUseCaseRequest) error {

	authErr := u.authService.AuthorizeByToken(req.Token)
	if authErr != nil {
		return authErr
	}

	var links []*entities.Link

	for _, protoLink := range req.ProtoLinks {
		newExternalId := u.idService.Generate()
		link := newLinkFromProtoLink(protoLink, entities.SourceInternal, newExternalId)
		links = append(links, link)
	}

	upsertErr := u.linksDAO.Upsert(links)
	if upsertErr != nil {
		return upsertErr
	}

	return nil
}

func NewAddNewLinksUseCase(linksDAO interfaces.ILinkDAO, idService interfaces.IIDService, authService *services.AuthService) *AddNewLinksUseCase {
	useCase := &AddNewLinksUseCase{}
	useCase.linksDAO = linksDAO
	useCase.idService = idService
	useCase.authService = authService
	return useCase
}

func newLinkFromProtoLink(protoLink *interfaces.ProtoLink, source entities.Source, externalId string) *entities.Link {
	id := entities.GenerateLinkID(source, externalId)
	return entities.NewLink(
		id,
		externalId,
		protoLink.Url,
		protoLink.Name,
		protoLink.Tags,
		source,
		time.Now(),
		"",
		"",
	)
}
