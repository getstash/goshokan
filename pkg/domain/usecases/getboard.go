package usecases

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"goshokan/pkg/domain/services"
)

type GetBoardUseCase struct {
	boardAggregateService *services.GetBoardWithLinksAggregateService
	authService           *services.AuthService
	public                bool
}

func (u *GetBoardUseCase) Execute(boardId string, token string) (*entities.BoardWithLinksAggregate, error) {
	if !u.public {
		err := u.authService.AuthorizeByToken(token)
		if err != nil {
			return nil, err
		}
	}

	board, err := u.boardAggregateService.GetById(boardId)
	if err != nil {
		return nil, err
	}
	if board == nil {
		return nil, interfaces.ErrNoBoard
	}

	if u.public && !board.IsOpen {
		return nil, interfaces.ErrNoBoard
	}

	return board, nil
}

func NewGetBoardUseCase(boardAggregateService *services.GetBoardWithLinksAggregateService, authService *services.AuthService, public bool) *GetBoardUseCase {
	return &GetBoardUseCase{
		boardAggregateService: boardAggregateService,
		authService:           authService,
		public:                public,
	}
}
