package entities

import "time"

type Token struct {
	IssuedAt time.Time
}
