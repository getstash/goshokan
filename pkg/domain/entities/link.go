package entities

import (
	"fmt"
	"time"
)

type Source int
type LinkID string

const (
	SourceInternal = 1
	SourcePocket   = 2
)

type Link struct {
	ID     LinkID
	Source Source
	Added  time.Time

	ExternalID string
	ImageUrl   string
	Url        string
	Excerpt    string
	Name       string
	Tags       []string
}

func NewLink(id LinkID, externalId string, url string, name string, tags []string, source Source, added time.Time, imageUrl string, excerpt string) *Link {
	return &Link{
		ID:         id,
		ExternalID: externalId,
		Url:        url,
		Name:       name,
		Tags:       tags,
		Source:     source,
		Added:      added,
		ImageUrl:   imageUrl,
		Excerpt:    excerpt,
	}
}

func GenerateLinkID(source Source, externalId string) LinkID {
	return LinkID(fmt.Sprintf("%d_%s", source, externalId))
}

func LinkIDListToStringList(linkIds []LinkID) []string {
	stringIds := []string{}
	for _, linkId := range linkIds {
		stringIds = append(stringIds, string(linkId))
	}
	return stringIds
}

func StringsListToLinkIDList(stringIds []string) []LinkID {
	linkIds := []LinkID{}
	for _, stringId := range stringIds {
		linkIds = append(linkIds, LinkID(stringId))
	}
	return linkIds
}

type LinkImage struct {
	Url    string
	Width  int
	Height int
}

type LinkImportInfo struct {
	Link   *Link
	Images []*LinkImage
}
