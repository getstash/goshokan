package entities

type Board struct {
	ID      string
	Name    string
	LinkIDs []LinkID
	IsOpen  bool
}

func NewBoard(id string, name string, linkIds []LinkID, isOpen bool) *Board {
	return &Board{
		ID:      id,
		Name:    name,
		LinkIDs: linkIds,
		IsOpen:  isOpen,
	}
}

type BoardWithLinksAggregate struct {
	ID     string
	Name   string
	Links  []*Link
	IsOpen bool
}

func NewBoardWithLinksAggregate(id string, name string, links []*Link, isOpen bool) *BoardWithLinksAggregate {
	return &BoardWithLinksAggregate{
		ID:     id,
		Name:   name,
		Links:  links,
		IsOpen: isOpen,
	}
}

func BoardWithLinksAggregateFromBoardAndLinks(board *Board, links []*Link) *BoardWithLinksAggregate {
	return &BoardWithLinksAggregate{
		ID:     board.ID,
		Name:   board.Name,
		Links:  links,
		IsOpen: board.IsOpen,
	}
}
