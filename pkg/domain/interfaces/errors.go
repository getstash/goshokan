package interfaces

import "errors"

var ErrNoBoard = errors.New("board not exists")

// ErrBadToken - is an error which produced by ITokenService when it cannot convert the token string into a Token
var ErrBadToken = errors.New("cannot parse token")
var ErrBadTokenCannotGetData = errors.New("cannot get data from token")
var ErrWrongCredentials = errors.New("wrong credentials")
