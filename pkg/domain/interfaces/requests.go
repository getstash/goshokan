package interfaces

import "goshokan/pkg/domain/entities"

type ProtoLink struct {
	Tags []string
	Name string
	Url  string
}

type AddNewLinkUseCaseRequest struct {
	ProtoLinks []*ProtoLink
	Token      string
}

type FindLinksUseCaseRequest struct {
	Query *FindLinkQuery
}

type NewBoard struct {
	Name    string
	LinkIDs []entities.LinkID
	IsOpen  bool
}

type AddNewBoardUseCaseRequest struct {
	NewBoard
	Token string
}

type UpdateBoardUseCaseRequest struct {
	NewBoard
	ID    string
	Token string
}

type GetBoardResponse struct {
	ID    string
	Name  string
	Links []*entities.Link
}
