package interfaces

type ILogger interface {
	ErrorWithMessage(err error, message string)
	Info(message string)
}
