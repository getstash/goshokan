package interfaces

import "goshokan/pkg/domain/entities"

type ISelectImageUseCase interface {
	Execute(info *entities.LinkImportInfo) *entities.LinkImportInfo
}
