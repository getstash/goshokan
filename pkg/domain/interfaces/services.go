package interfaces

import "goshokan/pkg/domain/entities"

type IIDService interface {
	Generate() string
}

type IPocketService interface {
	FetchAll() ([]*entities.LinkImportInfo, error)
}

//go:generate mockgen -destination=../../../tests/mocks/itokenservice_mock.go -package=mocks goshokan/pkg/domain/interfaces ITokenService
type ITokenService interface {
	ConvertTokenToString(token *entities.Token) (string, error)
	ConvertStringToToken(tokenString string) (*entities.Token, error)
}

type IImageProcessingService interface {
	FetchAndGetSize(string) (int, int, bool)
}
