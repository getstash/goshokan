package interfaces

import (
	"errors"
	"goshokan/pkg/domain/entities"
)

type ILinkDAOOrder int

const OrderByAddedDescending ILinkDAOOrder = 1

var ErrInvalidOrderConstant = errors.New("ILinkDAO: invalid order constant")

//go:generate mockgen -destination=../../../tests/mocks/ilinkdao_mock.go -package=mocks goshokan/pkg/domain/interfaces ILinkDAO
type ILinkDAO interface {
	Upsert([]*entities.Link) error
	FindByIds([]entities.LinkID) ([]*entities.Link, error)
	Search(pattern string, limit *int, offset *int, order *ILinkDAOOrder) ([]*entities.Link, error)
}

//go:generate mockgen -destination=../../../tests/mocks/iboarddao_mock.go -package=mocks goshokan/pkg/domain/interfaces IBoardDAO
type IBoardDAO interface {
	GetById(string) (*entities.Board, error)
	DeleteById(string) error
	UpsertOne(*entities.Board) error
	FindAll() ([]*entities.Board, error)
}

// DEPRECATED: Since project moved to elasticsearch

type Pagination struct {
	Limit  int
	Offset int
}

type FindLinkFilter struct {
	Tag *string
}

type FindLinkQuery struct {
	Filter     FindLinkFilter
	Pagination Pagination
}

type FindLinkResponse struct {
	Links      []*entities.Link
	Pagination Pagination
}
