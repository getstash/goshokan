package interfaces

import "goshokan/pkg/domain/entities"

type IFindSuitableImageTask interface {
	WaitUntilComplete()
	GetResult() []*entities.LinkImportInfo
	Run()
}

type ITaskFactory interface {
	FindSuitableImage([]*entities.LinkImportInfo) IFindSuitableImageTask
}
