package services

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/suite"
	"goshokan/pkg/domain/entities"
	"goshokan/tests/mocks"
	"testing"
	"time"
)

type AuthServiceTestSuite struct {
	suite.Suite

	service    *AuthService
	controller *gomock.Controller

	tokenServiceMock *mocks.MockITokenService
}

func (suite *AuthServiceTestSuite) SetupTest() {
	suite.controller = gomock.NewController(suite.T())

	suite.tokenServiceMock = mocks.NewMockITokenService(suite.controller)
	suite.service = NewAuthService(suite.tokenServiceMock, "test", "test")
}

func (suite *AuthServiceTestSuite) TestIsAuthorizedByBasic_CorrectCredentialsPassed_True() {

	result := suite.service.IsAuthorizedByBasic("test", "test")

	suite.Assert().Equal(true, result)
}

func (suite *AuthServiceTestSuite) TestIsAuthorizedByBasic_WrongCredentialsPassed_False() {

	result := suite.service.IsAuthorizedByBasic("wrong", "credentials")

	suite.Assert().Equal(false, result)
}

func (suite *AuthServiceTestSuite) TestAuthorizeByToken_TokenServiceReturAnError_TokenAuthErrorWithBadTokenReason() {

	tokenServiceErr := errors.New("dummy")

	suite.tokenServiceMock.EXPECT().ConvertStringToToken(gomock.Any()).Return(nil, tokenServiceErr)

	resultErr := suite.service.AuthorizeByToken("dummyToken")

	authErr, ok := resultErr.(*TokenAuthError)

	suite.Assert().True(ok, "Wrong type of error. Not a *TokenAuthError")
	suite.Assert().Equal(ReasonBadToken, authErr.GetReason())

}

func (suite *AuthServiceTestSuite) TestAuthorizeByToken_TokenExpired_TokenAuthErrorWithTooOldReason() {

	issuedAt := time.Now().Add(-1 * time.Duration((MaxTokenLifetime+1)*time.Second))
	token := entities.Token{IssuedAt: issuedAt}

	suite.tokenServiceMock.EXPECT().ConvertStringToToken(gomock.Any()).Return(&token, nil)

	resultErr := suite.service.AuthorizeByToken("dummyToken")

	authErr, ok := resultErr.(*TokenAuthError)

	suite.Assert().True(ok, "Wrong type of error. Not a *TokenAuthError")
	suite.Assert().Equal(ReasonTooOldToken, authErr.GetReason(), "Wrong error reason")

}

func (suite *AuthServiceTestSuite) TestAuthorizeByToken_AllCorrect_NoError() {

	issuedAt := time.Now().Add(-1 * time.Duration((MaxTokenLifetime-10)*time.Second))
	token := entities.Token{IssuedAt: issuedAt}

	suite.tokenServiceMock.EXPECT().ConvertStringToToken(gomock.Any()).Return(&token, nil)

	resultErr := suite.service.AuthorizeByToken("dummyToken")

	suite.Assert().Nil(resultErr, "There is must be no error")
}

func TestAuthService(t *testing.T) {
	testSuite := AuthServiceTestSuite{}
	suite.Run(t, &testSuite)
}
