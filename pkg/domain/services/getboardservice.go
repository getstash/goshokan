package services

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

type GetBoardWithLinksAggregateService struct {
	boardDAO interfaces.IBoardDAO
	linkDAO  interfaces.ILinkDAO
}

func (s *GetBoardWithLinksAggregateService) GetById(boardId string) (*entities.BoardWithLinksAggregate, error) {
	board, err := s.boardDAO.GetById(boardId)
	if err != nil {
		return nil, err
	}

	if board == nil {
		return nil, interfaces.ErrNoBoard
	}

	var links []*entities.Link

	if len(board.LinkIDs) != 0 {
		links, err = s.linkDAO.FindByIds(board.LinkIDs)
		if err != nil {
			return nil, err
		}
	}

	return entities.BoardWithLinksAggregateFromBoardAndLinks(board, links), nil
}

func NewGetBoardWithLinksAggregateService(boardDAO interfaces.IBoardDAO, linkDAO interfaces.ILinkDAO) *GetBoardWithLinksAggregateService {
	return &GetBoardWithLinksAggregateService{
		boardDAO: boardDAO,
		linkDAO:  linkDAO,
	}
}
