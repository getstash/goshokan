package services

import (
	"fmt"
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
	"time"
)

// MaxTokenLifetime is a token lifetime in seconds
const MaxTokenLifetime = 7 * 24 * 60 * 60

type TokenAuthErrorReason int

const ReasonBadToken TokenAuthErrorReason = 1
const ReasonTooOldToken TokenAuthErrorReason = 2

type TokenAuthError struct {
	reason TokenAuthErrorReason
}

func (t *TokenAuthError) Error() string {
	return fmt.Sprintf("token auth error. reason: %d", t.reason)
}

func (t *TokenAuthError) GetReason() TokenAuthErrorReason {
	return t.reason
}

func NewTokenAuthError(reason TokenAuthErrorReason) error {
	return &TokenAuthError{reason: reason}
}

type AuthService struct {
	tokenService interfaces.ITokenService

	AdminUsername string
	AdminPassword string
}

func (s *AuthService) IsAuthorizedByBasic(username, password string) bool {

	if username == s.AdminUsername && password == s.AdminPassword {
		return true
	}

	return false
}

func (s *AuthService) AuthorizeByToken(stringToken string) error {
	token, err := s.tokenService.ConvertStringToToken(stringToken)
	if err != nil {
		return NewTokenAuthError(ReasonBadToken)
	}

	if time.Now().Sub(token.IssuedAt).Seconds() > MaxTokenLifetime {
		return NewTokenAuthError(ReasonTooOldToken)
	}

	return nil
}

func (s *AuthService) GenerateStringToken() (string, error) {
	token := entities.Token{IssuedAt: time.Now()}
	return s.tokenService.ConvertTokenToString(&token)
}

func NewAuthService(tokenService interfaces.ITokenService, adminUsername, adminPassword string) *AuthService {
	return &AuthService{
		tokenService:  tokenService,
		AdminUsername: adminUsername,
		AdminPassword: adminPassword,
	}
}
