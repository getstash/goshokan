package services

import (
	"goshokan/pkg/domain/entities"
	"goshokan/pkg/domain/interfaces"
)

const MinHeight = 400
const MinWidth = 600

type ImageSelectionService struct {
	imageProcessingService interfaces.IImageProcessingService
}

func (s ImageSelectionService) SelectImageFromLinkInfo(images []*entities.LinkImage) *entities.LinkImage {

	var imagesToChoose []*entities.LinkImage
	for _, image := range images {

		if image.Width == 0 || image.Height == 0 {
			success := s.updateImageSize(image)
			if !success {
				continue
			}
		}

		imagesToChoose = append(imagesToChoose, image)
	}

	for _, image := range imagesToChoose {
		if image.Width >= MinWidth || image.Height >= MinHeight {
			return image
		}
	}

	return nil
}

func (s ImageSelectionService) updateImageSize(image *entities.LinkImage) bool {

	height, width, ok := s.imageProcessingService.FetchAndGetSize(image.Url)
	if ok {
		image.Height = height
		image.Width = width
	}

	return ok
}

func NewImageSelectionService(processingService interfaces.IImageProcessingService) *ImageSelectionService {
	return &ImageSelectionService{imageProcessingService: processingService}
}
