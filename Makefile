build: build-server build-pocket-import

build-server:
	go build -v ./cmd/server

build-pocket-import:
	go build -v ./cmd/pocket_import
